package com.lazyweaver.pg.bare.dto;

import java.util.ArrayList;
import java.util.List;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public class JobEntityList {

    private final List<JobEntity> jobs;

    public JobEntityList(List<JobEntity> jobs) {
        this.jobs = (jobs == null) ? new ArrayList<JobEntity>(0) : jobs;
    }

    public List<JobEntity> getJobs() {
        return jobs;
    }

    public int getSize() {
        return jobs.stream().filter(f -> f.getOperation() != null && f.getOperation().getStatus() != Status.CMD_ERROR).toArray().length;
    }
}
