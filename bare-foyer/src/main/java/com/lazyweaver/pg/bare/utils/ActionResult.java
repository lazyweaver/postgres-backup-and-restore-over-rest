package com.lazyweaver.pg.bare.utils;

import java.util.ArrayList;
import java.util.List;

public class ActionResult {

    private static final ActionResult OK = new ActionResult(null, true);
    private final String errorMessage;
    private final boolean isSuccess;
    private List<String> successResults;

    private ActionResult(String errorMessage, boolean isSuccess) {
        this.errorMessage = errorMessage;
        this.isSuccess = isSuccess;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public List<String> getSuccessResults() {
        return (successResults == null) ? new ArrayList<>(0) : successResults;
    }

    public static ActionResult success() {
        return OK;
    }

    public static ActionResult success(List<String> successResults) {
        ActionResult ok = new ActionResult(null, true);
        ok.successResults = successResults;
        return ok;
    }

    public static ActionResult fail(String message) {
        return new ActionResult(message, false);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName()).append(" [errorMessage=");
        builder.append(errorMessage);
        builder.append(", isSuccess=");
        builder.append(isSuccess);
        builder.append(", successResults=");
        builder.append(getSuccessResults());
        builder.append(']');
        return builder.toString();
    }
}
