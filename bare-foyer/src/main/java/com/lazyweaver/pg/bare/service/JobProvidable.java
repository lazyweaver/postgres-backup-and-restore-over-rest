package com.lazyweaver.pg.bare.service;

import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public interface JobProvidable {

    JobEntity getJobEntity(String... params);
}
