package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import com.lazyweaver.pg.bare.components.DateTime;
import com.lazyweaver.pg.bare.components.FilesystemManager;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.FileType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.components.JobManager;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.repository.JobFieldNames;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.JobProvidable;
import com.lazyweaver.pg.bare.utils.LocalOs;
import com.lazyweaver.pg.bare.utils.LocalOs.ExecCommandResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractJobOperations implements JobProvidable {

    protected final Logger log = LoggerFactory.getLogger(getClass());
    @Autowired
    protected JobManager jobManager;
    @Autowired
    protected FilesystemManager filesystemManager;
    @Autowired
    protected DateTime dateTime;
    @Autowired
    protected JobFieldNames jobFieldNames;

    @Override
    public final JobEntity getJobEntity(String... params) {
        if (params.length < 1) {
            JobEntity je = jobManager.getLast(getJobType());
            if (je != null) {
                return je;
            }
            log.info("Requested Last info not found for \"" + getJobType() + "\", will return from script status");
            OperationStatus status = getStatusByScript(getScriptFile().getName());
            return getJobEntity(status.getStatus(), status.getMessage());
        }
        return getPersistedJobEntity(params[0]);
    }

    protected JobEntity getJobEntityUnexpectedCondition(String scriptName) {
        OperationStatus currStatus = getStatusByScript(scriptName);
        if (currStatus.getStatus() == Status.CMD_ERROR) {
            log.info("Unexpected condition for job before execute : " + currStatus.getMessage());
            return getJobEntity(Status.CMD_ERROR, currStatus.getMessage());
        }
        if (currStatus.getStatus() != Status.INACTIVE) {
            log.warn("There is another operation of type \"" + getJobType() + "\" that is not in Inactive status : " + currStatus);
            return getJobEntity(Status.CMD_ERROR, "There is another operation of type \"" + getJobType() + "\" that is not in Inactive status : " + currStatus);
        }
        return null;
    }

    protected JobEntity getJobEntity(Status status, String message) {
        return new JobEntity(new OperationStatus(status, message)) {
            @Override
            public JobType getJobType() {
                return AbstractJobOperations.this.getJobType();
            }
        };
    }

    protected final JobEntity createJobEntity(LocalDateTime localDateTime, File archDir, File logsDir) {
        JobEntity jobEntity = new JobEntity() {
            @Override
            public JobType getJobType() {
                return AbstractJobOperations.this.getJobType();
            }
        };
        jobEntity.setId(UUID.randomUUID().toString());
        jobEntity.getData().put(jobFieldNames.getDateStarted(), dateTime.format(localDateTime));
        jobEntity.getData().put(jobFieldNames.getPath(), archDir.getAbsolutePath());
        jobEntity.getData().put(jobFieldNames.getLogs(), logsDir.getAbsolutePath());
        jobEntity.getData().put(jobFieldNames.getType(), jobEntity.getJobType());
        return jobEntity;
    }

    protected void executeCommand(String command, JobEntity jobEntity) {
        log.debug("Prepare to execute --> " + command);
        ExecCommandResult result = LocalOs.execCommand(command);
        if (!result.isSuccess()) {
            log.error("Command execution failed due to : " + result.getError() + ". Command : " + command);
            jobManager.remove(jobEntity);
            jobEntity.getOperation().setStatus(Status.CMD_ERROR);
            jobEntity.getOperation().setMessage(result.getError().toString());
            return;
        }
        final String successMsg = result.getOutput().isEmpty() ? "" : Arrays.toString(result.getOutput().toArray());
        log.debug("Command executed with success");
        jobManager.started(jobEntity);
        jobEntity.getOperation().setMessage(successMsg.isEmpty() ? "Successfully submitted" : successMsg);
        jobEntity.getOperation().setStatus(Status.ACTIVE);
    }

    protected abstract JobType getJobType();

    protected abstract FileType getEntityTypeScript();

    protected File getScriptFile() {
        return filesystemManager.getFile(filesystemManager.getDirectory(DirType.PgTools), getEntityTypeScript());
    }

    protected OperationStatus getStatusByScript(String scriptName) {
        ExecCommandResult result = LocalOs.execCommand("ps -a | grep " + scriptName + " | grep -v grep");
        if (!result.isSuccess()) {
            log.error("Unable to get \"" + scriptName + "\" status : " + result.getError().toString());
            return new OperationStatus(Status.CMD_ERROR, result.getError().toString());
        }
        for (String line : result.getOutput()) {
            if (line.contains(scriptName)) {
                return new OperationStatus(Status.ACTIVE, Arrays.toString(result.getOutput().toArray()));
            }
        }
        return new OperationStatus(Status.INACTIVE, result.getOutput().isEmpty() ? "" : Arrays.toString(result.getOutput().toArray()));
    }

    protected List<JobEntity> getActiveJobsByScriptName() {
        final String scriptName = getScriptFile().getName();
        final List<JobEntity> entities = new ArrayList<>();
        final ExecCommandResult result = LocalOs.execCommand("ps -a | grep " + scriptName + " | grep -v grep");
        if (!result.isSuccess()) {
            log.error("Unable to grep \"" + scriptName + "\" : " + result.getError().toString());
            entities.add(getJobEntity(Status.CMD_ERROR, result.getError().toString()));
            return entities;
        }
        for (String line : result.getOutput()) {
            if (line.contains(scriptName)) {
                JobEntity je = getJobEntity(Status.ACTIVE, line);
                tryToFindJobIdFromShellResponse(je, line);
                entities.add(je);
            }
        }
        return entities;
    }

    private JobEntity getPersistedJobEntity(String jobId) {
        JobEntity je = jobManager.getJobEntity(jobId);
        if (je.getId() == null || je.getJobType() == null) {
            return je;
        }
        if (je.getJobType() != getJobType()) {
            return getJobEntity(Status.UNKNOWN, "This job ID belongs to the type \"" + je.getJobType() + "\". Expected job type \"" + getJobType() + "\"");
        }
        return je;
    }

    private void tryToFindJobIdFromShellResponse(JobEntity je, String line) {
        String jobsPath = filesystemManager.getDirectory(DirType.JobsRoot).getAbsolutePath() + "/";
        if (!line.contains(jobsPath)) {
            return;
        }
        line = line.substring(line.indexOf(jobsPath) + jobsPath.length());
        int idxSpace = line.indexOf(" ");
        String jobId = (idxSpace < 1) ? line : line.substring(0, idxSpace);
        je.setId(jobId);
    }
}
