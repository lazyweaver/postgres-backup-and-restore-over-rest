package com.lazyweaver.pg.bare.repository;

public interface JobFieldNames {

    String getEndStatus();

    String getPath();

    String getPathRemote();

    String getArchive();

    String getLogs();

    String getLogsRemote();

    String getExecutionErrorLog();

    String getDateStarted();

    String getEndMessage();

    String getDateEnded();

    String getType();

    String getDatePattern();

    String getRequestParams();
}
