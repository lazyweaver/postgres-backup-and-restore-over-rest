package com.lazyweaver.pg.bare.repository.entity;

import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.dto.OperationStatus;

public class UnknownJobEntity extends JobEntity {

    public UnknownJobEntity(String message) {
        this(null, message);
    }

    public UnknownJobEntity(String jobId, String message) {
        super(jobId, new OperationStatus(Status.UNKNOWN, message));
    }

    @Override
    public JobType getJobType() {
        return null;
    }
}
