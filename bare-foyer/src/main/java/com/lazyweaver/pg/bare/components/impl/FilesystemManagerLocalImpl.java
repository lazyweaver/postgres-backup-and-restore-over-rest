package com.lazyweaver.pg.bare.components.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import com.lazyweaver.pg.bare.components.FilesystemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class FilesystemManagerLocalImpl implements FilesystemManager, InitializingBean {

	private static final Logger log = LoggerFactory.getLogger(FilesystemManagerLocalImpl.class);

	@Value("${postgres.backup.directory.local}")
	private String backupDirectoryLocal;
	@Value("${postgres.backup.directory.remote}")
	private String backupDirectoryRemote;
	@Value("${postgres.backup.directory.jobs}")
	private String backupJobsDirectoryName;
	@Value("${postgres.backup.directory.full}")
	private String backupFullDirectoryName;
	@Value("${postgres.backup.directory.partial}")
	private String backupPartialDirectoryName;

	@Value("${postgres.logs.directory.local}")
	private String logsDirectoryLocal;
	@Value("${postgres.logs.directory.remote}")
	private String logsDirectoryRemote;
	@Value("${postgres.logs.archiving.directory}")
	private String archivingDirectoryName;
	@Value("${postgres.logs.archiving.stdout.file}")
	private String logsStdoutFileName;
	@Value("${postgres.logs.archiving.stderr.file}")
	private String logsStderrFileName;

	@Value("${postgres.data.directory}")
	private String postgreDataDirectory;

	@Value("${postgres.tools.directory}")
	private String postgreToolsDirectory;
	@Value("${postgres.tools.backup.full}")
	private String postgreScriptBackupFull;
	@Value("${postgres.tools.backup.partial}")
	private String postgreScriptBackupPartial;
	@Value("${postgres.tools.restore.partial}")
	private String postgreScriptRestorePartial;
	@Value("${postgres.tools.restore.full}")
	private String postgreScriptRestoreFull;
	@Value("${postgres.tools.restore.pitr}")
	private String postgreScriptRestorePitr;

	private final Map<DirType, File> existingLocalAccessDirectories = new HashMap<>();
	private final Map<DirType, File> existingRemoteAccessDirectories = new HashMap<>();
	private final Map<FileType, String> fileNames = new HashMap<>();

	@Override
	public final void afterPropertiesSet() throws Exception {
		setLocalAccessDirectories();
		setRemoteAccessDirectories();
		setFileNames();
	}

	@Override
	public File getDirectory(DirType dirType) {
		return existingLocalAccessDirectories.get(dirType);
	}

	@Override
	public File getFile(File parentDir, FileType fileType) {
		return new File(parentDir, fileNames.get(fileType));
	}

	@Override
	public File createDirectoryStructure(DirType parentDirType, String directoryRelativePath) throws IOException {
		File newDirObj = new File(getDirectory(parentDirType), directoryRelativePath);
		if (newDirObj.isDirectory()) {
			log.error("Specified directory already exists : " + newDirObj.getAbsolutePath());
			throw new IOException("Specified directory already exists : " + newDirObj.getAbsolutePath());
		}
		if (!newDirObj.mkdirs()) {
			log.error("Specified directory can not be created : " + newDirObj.getAbsolutePath());
			throw new IOException("Specified directory can not be created : " + newDirObj.getAbsolutePath());
		}
		log.info("Created directory structure : " + newDirObj.getAbsolutePath());
		return newDirObj;
	}

	@Override
	public final String getDirectoryRelativePath(LocalDateTime localDateTime) {
		if (localDateTime == null) {
			throw new IllegalArgumentException("LocalDateTime missing mandatory parameter");
		}
		return localDateTime.getYear() + "/" + localDateTime.getMonthValue() + "/" + localDateTime.getDayOfMonth() + "/"
				+ localDateTime.getHour() + "/" + localDateTime.getMinute();
	}

	private static File createDirectoryIfNotExisting(File parent, String dirName) {
		File checkDir = new File(parent, dirName);
		if (checkDir.isDirectory()) {
			log.info("Directory exists : " + checkDir.getAbsolutePath());
			return checkDir;
		}
		if (!checkDir.mkdir()) {
			throw new IllegalStateException("Directory can not be created : " + checkDir.getAbsolutePath());
		} else {
			log.info("Directory created : " + checkDir.getAbsolutePath());
		}
		return checkDir;
	}

	private static File getValidDirectory(String path) {
		File checkDir = new File(path);
		if (!checkDir.isDirectory()) {
			log.error("Expected directory does NOT exist : " + checkDir.getAbsolutePath());
			throw new IllegalStateException("Expected directory does not exist : " + checkDir.getAbsolutePath());
		}
		log.info("Expected directory exists : " + checkDir.getAbsolutePath());
		return checkDir;
	}

	private void setLocalAccessDirectories() {
		File rootBackupDir = getValidDirectory(backupDirectoryLocal);
		File rootLogsDir = getValidDirectory(logsDirectoryLocal);
		existingLocalAccessDirectories.put(DirType.BackupRoot, rootBackupDir);
		existingLocalAccessDirectories.put(DirType.LogsRoot, rootLogsDir);
		existingLocalAccessDirectories.put(DirType.PgData, getValidDirectory(postgreDataDirectory));
		existingLocalAccessDirectories.put(DirType.PgTools, getValidDirectory(postgreToolsDirectory));
		existingLocalAccessDirectories.put(DirType.LogsArchiving,
				createDirectoryIfNotExisting(rootLogsDir, archivingDirectoryName));
		existingLocalAccessDirectories.put(DirType.JobsRoot,
				createDirectoryIfNotExisting(rootBackupDir, backupJobsDirectoryName));
		existingLocalAccessDirectories.put(DirType.BackupFull,
				createDirectoryIfNotExisting(rootBackupDir, backupFullDirectoryName));
		existingLocalAccessDirectories.put(DirType.BackupPartial,
				createDirectoryIfNotExisting(rootBackupDir, backupPartialDirectoryName));
	}

	private void setRemoteAccessDirectories() {
		File rootBackupDir = new File(backupDirectoryRemote);
		existingRemoteAccessDirectories.put(DirType.BackupRoot, rootBackupDir);
		existingRemoteAccessDirectories.put(DirType.LogsRoot, new File(logsDirectoryRemote));
		existingRemoteAccessDirectories.put(DirType.JobsRoot, new File(rootBackupDir, backupJobsDirectoryName));
	}

	private void setFileNames() {
		fileNames.put(FileType.StdOut, logsStdoutFileName);
		fileNames.put(FileType.StdErr, logsStderrFileName);
		fileNames.put(FileType.FullBackupScript, postgreScriptBackupFull);
		fileNames.put(FileType.FullRestoreScript, postgreScriptRestoreFull);
		fileNames.put(FileType.PartialBackupScript, postgreScriptBackupPartial);
		fileNames.put(FileType.PartialRestoreScript, postgreScriptRestorePartial);
		fileNames.put(FileType.PitrRestoreScript, postgreScriptRestorePitr);
	}

	@Override
	public String getRemoteAbsolutePath(File localFile, DirType dirType) {
		File localRoot = getDirectory(dirType);
		File remoteRoot = existingRemoteAccessDirectories.get(dirType);
		if (remoteRoot == null) {
			throw new IllegalArgumentException("No Remote access directory configured for dirType=\"" + dirType + "\"");
		}
		return localFile.getAbsolutePath().replace(localRoot.getAbsolutePath(), remoteRoot.getAbsolutePath());
	}
}
