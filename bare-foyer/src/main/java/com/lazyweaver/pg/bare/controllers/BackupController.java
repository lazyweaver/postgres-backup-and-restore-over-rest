package com.lazyweaver.pg.bare.controllers;

import com.lazyweaver.pg.bare.dto.JobEntityList;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.PostgreFullBackupService;
import com.lazyweaver.pg.bare.service.PostgrePartialBackupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/backup")
@Api(tags = "Backup")
public class BackupController {

    @Autowired
    private PostgreFullBackupService fullBackupService;
    @Autowired
    private PostgrePartialBackupService partialBackupService;

    @GetMapping(value = "/full")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of the Last \"Full Backup\" job")
    public ResponseEntity<JobEntity> getLastFullBackupJob() {
        return ResponseEntity.ok(fullBackupService.getJobEntity());
    }

    @GetMapping(value = "/full/{jobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of a \"Full Backup\" job by ID")
    public ResponseEntity<JobEntity> getFullBackupJob(@PathVariable("jobId") String jobId) {
        return ResponseEntity.ok(fullBackupService.getJobEntity(jobId));
    }

    @PostMapping(value = "/full")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Creates a new \"Full Backup\" job", notes = "On success an entity with a generated ID is returned and status ACTIVE")
    public ResponseEntity<JobEntity> createFullBackup() {
        return ResponseEntity.ok(fullBackupService.createBackup());
    }

    @GetMapping(value = "/partial/active")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Gets all active \"Partial Backup\" jobs")
    public ResponseEntity<JobEntityList> getActivePartialBackupJobs() {
        return ResponseEntity.ok(new JobEntityList(partialBackupService.getActiveJobs()));
    }

    @GetMapping(value = "/partial")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of the Last \"Partial Backup\" job")
    public ResponseEntity<JobEntity> getLastPartialBackupJob() {
        return ResponseEntity.ok(partialBackupService.getJobEntity());
    }

    @GetMapping(value = "/partial/{jobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of a \"Partial Backup\" job by ID")
    public ResponseEntity<JobEntity> getPartialBackupJob(@ApiParam("Job ID") @PathVariable("jobId") String jobId) {
        return ResponseEntity.ok(partialBackupService.getJobEntity(jobId));
    }

    @PostMapping(value = "/partial/table/{tableName}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Creates a new \"Partial Backup\" job for \"table\"", notes = "On success an entity with a generated ID is returned and status ACTIVE")
    public ResponseEntity<JobEntity> createBackupForTable(@ApiParam("table name") @PathVariable("tableName") String tableName) {
        return ResponseEntity.ok(partialBackupService.createBackup("table", tableName));
    }

    @PostMapping(value = "/partial/schema/{schemaName}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Creates a new \"Partial Backup\" job for \"schema\"", notes = "On success an entity with a generated ID is returned and status ACTIVE")
    public ResponseEntity<JobEntity> createBackupForSchema(@ApiParam("schema name") @PathVariable("schemaName") String schemaName) {
        return ResponseEntity.ok(partialBackupService.createBackup("schema", schemaName));
    }
}
