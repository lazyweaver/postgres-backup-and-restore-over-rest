package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDateTime;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.FileType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.PostgrePitrRestoreService;
import com.lazyweaver.pg.bare.utils.LocalOs;
import com.lazyweaver.pg.bare.utils.LocalOs.ExecCommandResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PostgrePitrRestoreServiceLocalOsImpl extends AbstractRestoreOperations implements PostgrePitrRestoreService {

    @Value("${postgres.restore.pitr.date.format.recoveryconf}")
    private String datetimeFormatRecoveryconf;

    @Override
    public JobEntity createRestore(String toDateString) {
        LocalDateTime requestedDateTime = null;
        try {
            requestedDateTime = dateTime.parse(toDateString);
        } catch (Exception e) {
            String msg = "The supplied date-time \"" + toDateString + "\" is not matching pattern \"" + dateTime.getFormatPattern() + "\"";
            log.info(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        if (requestedDateTime.isAfter(dateTime.now())) {
            String msg = "The supplied date-time \"" + toDateString + "\" is in the future";
            log.info(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        String sourceJobId = null;
        try {
            sourceJobId = findLastFullBackupBeforeDateTime(requestedDateTime);
        } catch (Exception e) {
            String msg = "Unable to find suitable \"" + getSourceJobTypeForRestore() + "\" before date \"" + dateTime.format(requestedDateTime) + "\" due to : " + e.getMessage();
            log.info(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        return createRestoreJobAndExecute(sourceJobId, dateTime.format(requestedDateTime, datetimeFormatRecoveryconf) + " UTC+3");
    }

    @Override
    protected JobType getSourceJobTypeForRestore() {
        return JobType.FullBackup;
    }

    @Override
    protected String getCommand(File archDir, File logsDir, JobEntity entity, String... params) {
        if (params == null || params.length < 1) {
            throw new IllegalArgumentException("Expected at least 1 parameter for date");
        }
        StringBuilder cmd = new StringBuilder();
        cmd.append(getScriptFile().getAbsolutePath());
        cmd.append(" \"").append(filesystemManager.getDirectory(DirType.PgData).getAbsolutePath()).append('"');
        cmd.append(" \"").append(archDir.getAbsolutePath()).append('"');
        cmd.append(" \"").append(filesystemManager.getDirectory(DirType.LogsRoot).getAbsolutePath()).append('"');
        cmd.append(" \"").append(params[0]).append('"');
        cmd.append(" \"").append(jobManager.getStorage(entity)).append('"');
        cmd.append(" > ").append(filesystemManager.getFile(logsDir, FileType.StdOut).getAbsolutePath());
        cmd.append(" 2> ").append(filesystemManager.getFile(logsDir, FileType.StdErr).getAbsolutePath());
        cmd.append(" &");
        return cmd.toString();
    }

    @Override
    protected JobType getJobType() {
        return JobType.PitrRestore;
    }

    @Override
    protected FileType getEntityTypeScript() {
        return FileType.PitrRestoreScript;
    }

    private String findLastFullBackupBeforeDateTime(LocalDateTime requestedDateTime) throws IOException {
        final StringBuilder cmd = new StringBuilder();
        final String jobsRootPath = filesystemManager.getDirectory(DirType.JobsRoot).getAbsolutePath();
        cmd.append("grep -x -l 'type=").append(getSourceJobTypeForRestore()).append("' ").append(jobsRootPath).append("/* | while read -r fileName ; do");
        cmd.append(" lineRes=$(grep -x -l 'end_status=success' $fileName);");
        cmd.append(" if [ $? -eq 0 ];then grep -H 'date_ended=' $lineRes; fi ");
        cmd.append("done");
        ExecCommandResult result = LocalOs.execCommand(cmd.toString());
        if (!result.isSuccess()) {
            throw new IOException("Command for finding last successful backup failed with : " + result.getError());
        }
        String jobId = null;
        LocalDateTime bestBackupDateTime = null;
        for (String line : result.getOutput()) {
            int idx = line.indexOf(":");
            if (idx < 1) {
                log.error("Unexpected output line from grep for finding last success full backup :" + line);
                continue;
            }
            String jobIdTemp = line.substring(0, idx).replace(jobsRootPath + "/", "");
            String dateTimeTemp = line.substring(idx + 1).replace("date_ended=", "");
            LocalDateTime backupDateTime = null;
            try {
                backupDateTime = dateTime.parse(dateTimeTemp);
            } catch (Exception e) {
                log.error("Unexpected output line from grep for finding last success full backup : date-time \"" + dateTimeTemp
                        + "\" can not be parsed using pattern \"" + dateTime.getFormatPattern() + "\". line=>" + line);
                continue;
            }
            if (backupDateTime.isBefore(requestedDateTime) && (bestBackupDateTime == null || backupDateTime.isAfter(bestBackupDateTime))) {
                bestBackupDateTime = backupDateTime;
                jobId = jobIdTemp;
            }
        }
        if (jobId == null) {
            throw new FileNotFoundException("No such Job record exists");
        }
        return jobId;
    }
}
