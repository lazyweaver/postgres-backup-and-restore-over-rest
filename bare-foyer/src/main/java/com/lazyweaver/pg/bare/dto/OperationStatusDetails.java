package com.lazyweaver.pg.bare.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OperationStatusDetails extends OperationStatus {

    private final List<Map<String, Object>> details = new ArrayList<>();

    public OperationStatusDetails(Status status, String message) {
        super(status, message);
    }

    public void add(Map<String, Object> detail) {
        details.add(detail);
    }

    public List<Map<String, Object>> getDetails() {
        return details;
    }
}
