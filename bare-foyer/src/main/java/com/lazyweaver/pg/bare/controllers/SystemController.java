package com.lazyweaver.pg.bare.controllers;

import com.lazyweaver.pg.bare.components.JobManager;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/system")
@Api(tags = "System")
public class SystemController {

    @Autowired
    private SystemService systemService;
    @Autowired
    private JobManager jobManager;

    @GetMapping(value = "/database")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides the status of PostgreSQL Server", notes = "Expected entities with status values - ACTIVE, INACTIVE")
    public ResponseEntity<OperationStatus> getDatabaseServerStatus() {
        return getResponseEntity(systemService.getDatabaseStatus());
    }

    @GetMapping(value = "/bare-foyer")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides the status of Bare Foyer Service and directories", notes = "Expected entities with status values - ACTIVE, INACTIVE and directories details")
    public ResponseEntity<OperationStatus> getBareFoyerStatus() {
        return getResponseEntity(systemService.getBareFoyerStatus());
    }

    @GetMapping(value = "/job/{jobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information on any job by ID")
    public ResponseEntity<JobEntity> getJobEntity(@PathVariable("jobId") String jobId) {
        return ResponseEntity.ok(jobManager.getJobEntity(jobId));
    }

    @DeleteMapping(value = "/job/{jobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Deletes all related data and the job by ID")
    public ResponseEntity<OperationStatus> removeJobEntity(@ApiParam("Job ID of any type") @PathVariable("jobId") String jobId) {
        return getResponseEntity(jobManager.remove(jobId));
    }

    private static ResponseEntity<OperationStatus> getResponseEntity(OperationStatus status) {
        if (status.getStatus() == Status.INACTIVE || status.getStatus() == Status.UNKNOWN) {
            return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(status);
        }
        if (status.getStatus() == Status.CMD_ERROR) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(status);
        }
        return ResponseEntity.ok(status);
    }
}
