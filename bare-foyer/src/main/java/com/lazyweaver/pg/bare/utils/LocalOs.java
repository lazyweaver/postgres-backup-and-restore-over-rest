package com.lazyweaver.pg.bare.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public final class LocalOs {

    private static final boolean isWindows = System.getProperty("os.name").startsWith("Windows");
    private static final boolean isMac = System.getProperty("os.name").startsWith("Mac");
    private static final boolean isLinux = System.getProperty("os.name").toLowerCase().contains("linux");
    private static final boolean isUnix;
    static {
        String os = System.getProperty("os.name").toLowerCase();
        isUnix = (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0 || os.indexOf("sunos") > 0 || os.indexOf("bsd") > 0);
    }

    public static ExecCommandResult execCommand(String... commands) {
        return execCommand(null, commands);
    }

    public static ExecCommandResult execCommand(ProgressLogger progressLogger, String... commands) {
        ProcessBuilder builder = new ProcessBuilder(getShell(commands));
        builder.redirectErrorStream(false);
        BufferedReader brOk = null;
        Reader readerOk = null;
        BufferedReader brKo = null;
        Reader readerKo = null;
        final ExecCommandResult result = new ExecCommandResult();
        try {
            Process p = builder.start();
            readerOk = new InputStreamReader(p.getInputStream());
            readerKo = new InputStreamReader(p.getErrorStream());
            brOk = new BufferedReader(readerOk);
            brKo = new BufferedReader(readerKo);

            BufferedReaderThread threadKO = new BufferedReaderThread(brKo, progressLogger, (log) -> result.error.append(log).append(" "));
            BufferedReaderThread threadOK = new BufferedReaderThread(brOk, progressLogger, (log) -> result.output.add(log));
            ThreadUtils.setThreadNamePreserveCounter(threadKO, "localos-command-exec-read-error");
            ThreadUtils.setThreadNamePreserveCounter(threadOK, "localos-command-exec-read-output");
            threadKO.start();
            threadOK.start();
            threadKO.join();
            threadOK.join();

        } catch (Exception e) {
            e.printStackTrace();
            result.error.append(e.getMessage());
        } finally {
            IoUtils.close(readerOk, readerKo, brOk, brKo);
        }
        return result;
    }

    public static boolean isWindows() {
        return isWindows;
    }

    public static boolean isMac() {
        return isMac;
    }

    public static boolean isLinux() {
        return isLinux;
    }

    private static String[] getShell(String... commands) {
        String command = asOneLineCommand(commands);
        if (isWindows) {
            return new String[] { "cmd", "/C", command };
        }
        if (isLinux || isUnix) {
            return new String[] { "sh", "-c", command };
        }
        if (isMac) {
            // Mac shell is a no-login one, paths and other variables are not loaded by default
            return new String[] { "sh", "-c", "source /etc/profile;" + command };
        }
        return null;
    }

    private static String asOneLineCommand(String[] commands) {
        final String commandSeparator = isWindows() ? "&&" : ";";
        final boolean isMultipleCommands = commands.length > 1;
        StringBuilder bld = new StringBuilder();
        for (String command : commands) {
            bld.append(commandSeparator);
            if (isMultipleCommands) {
                bld.append('(');
            }
            bld.append(command);
            if (isMultipleCommands) {
                bld.append(')');
            }
        }
        return bld.length() > commandSeparator.length() ? bld.substring(commandSeparator.length()) : bld.toString();
    }

    public static class ExecCommandResult {

        private final List<String> output;
        private final StringBuilder error;

        private ExecCommandResult() {
            output = new ArrayList<>();
            error = new StringBuilder();
        }

        public boolean isSuccess() {
            return error.length() == 0;
        }

        public List<String> getOutput() {
            return output;
        }

        public StringBuilder getError() {
            return error;
        }
    }
}
