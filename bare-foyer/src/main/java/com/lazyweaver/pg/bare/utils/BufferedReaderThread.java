package com.lazyweaver.pg.bare.utils;

import java.io.BufferedReader;

public class BufferedReaderThread extends Thread {

    private final BufferedReader bufferedReader;
    private final ProgressLogger[] progressLoggers;

    public BufferedReaderThread(BufferedReader bufferedReader, ProgressLogger... progressLoggers) {
        this.bufferedReader = bufferedReader;
        this.progressLoggers = progressLoggers;
    }

    @Override
    public void run() {
        String line = null;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                if (progressLoggers != null) {
                    for (ProgressLogger progressLogger : progressLoggers) {
                        if (progressLogger != null) {
                            progressLogger.log(line);
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Unable to read from bufferedReader due to : " + e.getMessage());
        }
    }
}
