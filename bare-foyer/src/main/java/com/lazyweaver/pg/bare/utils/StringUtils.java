package com.lazyweaver.pg.bare.utils;

import java.text.MessageFormat;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

public final class StringUtils {

    private StringUtils() {}

    public static boolean isEmpty(String test) {
        return test == null || test.trim().isEmpty();
    }

    public static int toInt(String test, int defaultInt) {
        if (test == null || test.isEmpty()) {
            return defaultInt;
        }
        int parsedInt = defaultInt;
        try {
            parsedInt = Integer.parseInt(test.trim());
        } catch (NumberFormatException nfe) {
            // expected
        }
        return parsedInt;
    }

    public static String capitalizeFirstLetter(String string) {
        if (isEmpty(string)) {
            return string;
        }
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }

    /**
     * Returns the supplied string as lower-case and removed non-alpha chars
     * 
     * @param name
     * @return alpha string
     */
    public static String toAlphaLowercase(String text) {
        if (text == null || text.isEmpty()) {
            return text;
        }
        String normalized = Normalizer.normalize(text, Form.NFD);
        return normalized.replaceAll("[^A-Za-z]", "").toLowerCase();
    }

    public static boolean isUnixDirectoryCompliant(String test) {
        if (isEmpty(test) || test.equals("/") || test.equals("/root") || !test.startsWith("/") || test.contains("\\")) {
            return false;
        }
        for (StringTokenizer st = new StringTokenizer(test, "/"); st.hasMoreTokens();) {
            String dirTest = st.nextToken().replace(".", "");
            if (dirTest.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public static String getFormattedText(ResourceBundle rb, String key, Object... arguments) {
        return MessageFormat.format(rb.getString(key), arguments);
    }
}
