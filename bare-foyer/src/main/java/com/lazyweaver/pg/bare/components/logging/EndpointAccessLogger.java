package com.lazyweaver.pg.bare.components.logging;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class EndpointAccessLogger implements Filter {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final ObjectMapper objectMapper;
    
    public EndpointAccessLogger(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        ApiCallInfo info = getApiCallInfo(request);
        if (info == null) {
            chain.doFilter(request, response);
            return;
        }
        final long startTime = System.currentTimeMillis();
        chain.doFilter(request, response);
        info.durationMiliseconds = System.currentTimeMillis() - startTime;
        log.info(getAsJsonMessageOrDefault(info));
    }

    private static ApiCallInfo getApiCallInfo(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            return null;
        }
        ApiCallInfo info = new ApiCallInfo();
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        info.url = httpRequest.getRequestURL().toString();
        if (!isApiUrl(info.url)) {
            return null;
        }
        info.method = httpRequest.getMethod();
        info.parameters = httpRequest.getParameterMap();
        return info;
    }

    private static boolean isApiUrl(String url) {
        return url.contains("/system/") || url.contains("/backup/") || url.contains("/restore/");
    }

    private String getAsJsonMessageOrDefault(ApiCallInfo info) {
        String message = null;
        try {
            message = "{API_CALL}" + objectMapper.writeValueAsString(info);
        } catch (Exception e) {
            log.error("Unable to serialize object for logging purposes due to : " + e.getMessage(), e);
            message = String.format("API call request took {} miliseconds", info.durationMiliseconds);
        }
        return message;
    }

    private static class ApiCallInfo {
        private String url;
        private long durationMiliseconds = -1;
        private String method;
        private Map<String, String[]> parameters;

        public String getUrl() {
            return url == null ? "n/a" : url;
        }

        public String getMethod() {
            return method == null ? "n/a" : method;
        }

        public Map<String, String[]> getParameters() {
            return parameters == null ? new HashMap<String, String[]>(0) : parameters;
        }

        public long getDurationMiliseconds() {
            return durationMiliseconds;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() + " [url=" + getUrl() + ", durationMiliseconds = " + getDurationMiliseconds() + ", method=" + getMethod() + ", parameters=" + getParameters().keySet().stream().map(key -> key + "=" + getParameters().get(key)).collect(Collectors.joining(", ", "{", "}")) + "]";
        }
    }
}
