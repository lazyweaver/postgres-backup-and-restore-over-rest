package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Properties;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.utils.IoUtils;

public abstract class AbstractRestoreOperations extends AbstractJobOperations {

    protected JobEntity createRestoreJobAndExecute(String sourceJobId, String... params) {
        JobEntity unexpectedConditionEntity = getJobEntityUnexpectedCondition(getScriptFile().getName());
        if (unexpectedConditionEntity != null) {
            return unexpectedConditionEntity;
        }
        File sourceJobFile = new File(jobManager.getStorage(sourceJobId));
        if (!sourceJobFile.isFile()) {
            String msg = "No such source job record. Missing job file for ID \"" + sourceJobId + "\" under path : " + sourceJobFile.getAbsolutePath();
            log.warn(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        Properties sourceJobProps = null;
        try {
            sourceJobProps = IoUtils.loadExternalProperties(sourceJobFile);
        } catch (Exception e) {
            String msg = "Unable to load Properties for job ID \"" + sourceJobId + "\" from : " + sourceJobFile.getAbsolutePath();
            log.warn(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        final File archiveDir = new File(sourceJobProps.getProperty(jobFieldNames.getPath()));
        if (!archiveDir.isDirectory()) {
            String msg = "Source archive directory for ID \"" + sourceJobId + "\" not found : " + archiveDir.getAbsolutePath();
            log.warn(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        log.debug("Archive directory for ID \"" + sourceJobId + "\" exists : " + archiveDir.getAbsolutePath());
        if (!"success".equals(sourceJobProps.get(jobFieldNames.getEndStatus()))) {
            String msg = "Source archive Backup for ID \"" + sourceJobId + "\" has ended with : " + sourceJobProps.get(jobFieldNames.getEndStatus());
            log.warn(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        try {
            validateSourceJobExpectedData(sourceJobProps);
        } catch (Exception e) {
            String msg = "Source job validation error : " + e.getMessage();
            log.warn(msg);
            return getJobEntity(Status.CMD_ERROR, msg);
        }
        final LocalDateTime now = dateTime.now();
        File newLogsDirObj;
        try {
            newLogsDirObj = filesystemManager.createDirectoryStructure(DirType.LogsArchiving, filesystemManager.getDirectoryRelativePath(now));
        } catch (IOException e) {
            log.error("Unable to create Restore Logs directory structure", e);
            return getJobEntity(Status.CMD_ERROR, e.getMessage());
        }

        JobEntity je = createJobEntity(now, archiveDir, newLogsDirObj);
        jobManager.persist(je);
        log.info("New restore job persisted " + je.getId());
        executeCommand(getCommand(archiveDir, newLogsDirObj, je, params), je);
        return je;
    }

    private void validateSourceJobExpectedData(Properties jobPros) throws Exception {
        String realSourceJobType = jobPros.getProperty(jobFieldNames.getType());
        String expectedSourceJobType = getSourceJobTypeForRestore().name();
        if (!expectedSourceJobType.equals(realSourceJobType)) {
            throw new IllegalArgumentException("Expected source job type \"" + expectedSourceJobType + "\", but was \"" + realSourceJobType + "\"");
        }
    }

    protected abstract JobType getSourceJobTypeForRestore();
    protected abstract String getCommand(File backupDir, File logsDir, JobEntity entity, String... params);
}
