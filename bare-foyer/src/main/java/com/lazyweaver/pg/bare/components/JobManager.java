package com.lazyweaver.pg.bare.components;

import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public interface JobManager {

    JobEntity getJobEntity(String jobId);

    void persist(JobEntity jobEntity);

    String getStorage(JobEntity entity);

    String getStorage(String entityId);

    OperationStatus remove(String entityId);

    OperationStatus remove(JobEntity jobEntity);

    void started(JobEntity jobEntity);

    JobEntity getLast(JobType jobType);
}
