package com.lazyweaver.pg.bare.dto;

public abstract class AbstractStatus {

    public enum Status {
        INACTIVE, ACTIVE, INACTIVE_MAINTENANCE, UNKNOWN, FINISHED, CMD_ERROR
    };

    private Status status;

    public AbstractStatus(Status status) {
        this.status = status;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [status=" + status + "]";
    }
}
