package com.lazyweaver.pg.bare.repository.impl;

import com.lazyweaver.pg.bare.repository.JobFieldNames;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class JobFieldNamesImpl implements JobFieldNames {

    @Value("${postgres.job.field.path}")
    private String jobFieldPath;
    @Value("${postgres.job.field.path.remote}")
    private String jobFieldPathRemote;
    @Value("${postgres.job.field.logs}")
    private String jobFieldLogs;
    @Value("${postgres.job.field.logs.remote}")
    private String jobFieldLogsRemote;
    @Value("${postgres.job.field.type}")
    private String jobFieldType;
    @Value("${postgres.job.field.date_pattern}")
    private String jobFieldDatePattern;
    @Value("${postgres.job.field.date_started}")
    private String jobFieldDateStarted;
    @Value("${postgres.job.field.date_ended}")
    private String jobFieldDateEnded;
    @Value("${postgres.job.field.archive}")
    private String jobFieldArchive;
    @Value("${postgres.job.field.end_status}")
    private String jobFieldEndStatus;
    @Value("${postgres.job.field.end_message}")
    private String jobFieldEndMessage;
    @Value("${postgres.job.field.execution_error_log}")
    private String jobFieldExecutionErrorLog;
    @Value("${postgres.job.field.request_params}")
    private String jobFieldRequestParams;

    @Override
    public String getEndStatus() {
        return jobFieldEndStatus;
    }

    @Override
    public String getPath() {
        return jobFieldPath;
    }

    @Override
    public String getPathRemote() {
        return jobFieldPathRemote;
    }

    @Override
    public String getArchive() {
        return jobFieldArchive;
    }

    @Override
    public String getLogs() {
        return jobFieldLogs;
    }

    @Override
    public String getLogsRemote() {
        return jobFieldLogsRemote;
    }

    @Override
    public String getExecutionErrorLog() {
        return jobFieldExecutionErrorLog;
    }

    @Override
    public String getEndMessage() {
        return jobFieldEndMessage;
    }

    @Override
    public String getDateEnded() {
        return jobFieldDateEnded;
    }

    @Override
    public String getDateStarted() {
        return jobFieldDateStarted;
    }

    @Override
    public String getType() {
        return jobFieldType;
    }

    @Override
    public String getDatePattern() {
        return jobFieldDatePattern;
    }

    @Override
    public String getRequestParams() {
        return jobFieldRequestParams;
    }
}
