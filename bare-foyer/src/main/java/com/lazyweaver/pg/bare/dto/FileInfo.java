package com.lazyweaver.pg.bare.dto;

public class FileInfo {

    private final String type;
    private final String path;
    private final long sizeBytes;

    public FileInfo(String type, String path, long sizeBytes) {
        this.type = type;
        this.path = path;
        this.sizeBytes = sizeBytes;
    }

    public String getPath() {
        return path;
    }

    public long getSizeBytes() {
        return sizeBytes;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [type=" + type + ", path=" + path + ", sizeBytes=" + sizeBytes + "]";
    }
}
