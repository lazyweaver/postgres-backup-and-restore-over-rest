package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import java.util.List;
import com.lazyweaver.pg.bare.components.FilesystemManager.FileType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.PostgrePartialRestoreService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PostgrePartialRestoreServiceLocalOsImpl extends AbstractRestoreOperations implements PostgrePartialRestoreService {

    @Value("${postgres.backup.db.main}")
    private String mainDatabaseName;

    @Override
    public List<JobEntity> getActiveJobs() {
        return getActiveJobsByScriptName();
    }
   
    @Override
    public JobEntity createRestore(String jobId) {
        return createRestoreJobAndExecute(jobId);
    }

    @Override
    protected JobType getJobType() {
        return JobType.PartialRestore;
    }

    @Override
    protected JobType getSourceJobTypeForRestore() {
        return JobType.PartialBackup;
    }

    @Override
    protected FileType getEntityTypeScript() {
        return FileType.PartialRestoreScript;
    }

    @Override
    protected String getCommand(File archDir, File logsDir, JobEntity entity, String... params) {
        StringBuilder cmd = new StringBuilder();
        cmd.append(getScriptFile().getAbsolutePath());
        cmd.append(" \"").append(mainDatabaseName).append('"');
        cmd.append(" \"").append(archDir.getAbsolutePath()).append('"');
        cmd.append(" \"").append(filesystemManager.getFile(logsDir, FileType.StdErr).getAbsolutePath()).append('"');
        cmd.append(" \"").append(jobManager.getStorage(entity)).append('"');
        cmd.append(" > ").append(filesystemManager.getFile(logsDir, FileType.StdOut).getAbsolutePath());
        cmd.append(" 2> ").append(filesystemManager.getFile(logsDir, FileType.StdErr).getAbsolutePath());
        cmd.append(" &");
        return cmd.toString();
    }
}
