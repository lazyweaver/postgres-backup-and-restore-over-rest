package com.lazyweaver.pg.bare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BareFoyerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BareFoyerApplication.class, args);
    }
}
