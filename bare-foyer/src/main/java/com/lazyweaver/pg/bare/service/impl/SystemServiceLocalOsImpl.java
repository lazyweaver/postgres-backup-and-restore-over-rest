package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import com.lazyweaver.pg.bare.components.FilesystemManager;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.components.JobManager;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.dto.OperationStatusDetails;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.SystemService;
import com.lazyweaver.pg.bare.utils.LocalOs;
import com.lazyweaver.pg.bare.utils.LocalOs.ExecCommandResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SystemServiceLocalOsImpl implements SystemService {

    private final Logger log = LoggerFactory.getLogger(getClass());
    private static final String NO_SERVER_RUNNING = "no server running";

    private final FilesystemManager filesystemManager;
    private final JobManager jobManager;

    public SystemServiceLocalOsImpl(FilesystemManager filesystemManager, JobManager jobManager) {
        this.filesystemManager = filesystemManager;
        this.jobManager = jobManager;
    }

    @Override
    public OperationStatus getDatabaseStatus() {
        ExecCommandResult result = LocalOs.execCommand("pg_ctl status");
        if (!result.isSuccess()) {
            if (result.getError().toString().contains(NO_SERVER_RUNNING)) {
                if (isExpectedInactivity()) {
                    return new OperationStatus(OperationStatus.Status.INACTIVE_MAINTENANCE, "DB maintenance in progress. Additional command error:" + result.getError().toString());
                }
                return new OperationStatus(OperationStatus.Status.INACTIVE, result.getError().toString());
            }
            return new OperationStatus(OperationStatus.Status.CMD_ERROR, result.getError().toString());
        }
        String commandOutput = Arrays.toString(result.getOutput().toArray());
        for (String line : result.getOutput()) {
            if (line.contains("running") && line.contains("PID")) {
                return new OperationStatus(OperationStatus.Status.ACTIVE, commandOutput);
            }
            if (line.contains(NO_SERVER_RUNNING)) {
                if (isExpectedInactivity()) {
                    return new OperationStatus(OperationStatus.Status.INACTIVE_MAINTENANCE, "DB maintenance in progress");
                }
                return new OperationStatus(OperationStatus.Status.INACTIVE, commandOutput);
            }
        }
        log.error("Undetermined command output : " + commandOutput);
        return new OperationStatus(OperationStatus.Status.UNKNOWN, commandOutput);
    }

    @Override
    public OperationStatus getBareFoyerStatus() {
        ExecCommandResult result = LocalOs.execCommand("ps -a | grep bare-foyer | grep -v grep");
        if (!result.isSuccess()) {
            log.error("Unable to grep service status due to : " + result.getError().toString());
            return new OperationStatus(OperationStatus.Status.CMD_ERROR, result.getError().toString());
        }
        String commandOutput = Arrays.toString(result.getOutput().toArray());
        log.debug("grep service returned : " + String.join("\n", result.getOutput()));
        for (String line : result.getOutput()) {
            if (line.contains("java") && line.contains("bare-foyer")) {
                OperationStatusDetails osDetails = new OperationStatusDetails(OperationStatus.Status.ACTIVE, commandOutput);
                osDetails.add(getDirectoryDetails(DirType.BackupRoot));
                osDetails.add(getDirectoryDetails(DirType.LogsRoot));
                osDetails.add(getDirectoryDetails(DirType.JobsRoot));
                return osDetails;
            }
        }
        log.error("Undetermined command output : " + commandOutput);
        return new OperationStatus(OperationStatus.Status.UNKNOWN, commandOutput);
    }

    private Map<String, Object> getDirectoryDetails(DirType dirType) {
        Map<String, Object> dirDetails = new HashMap<>();
        File dir = filesystemManager.getDirectory(dirType);
        dirDetails.put("type", dirType.name());
        dirDetails.put("pathLocal", dir.getAbsolutePath());
        dirDetails.put("pathRemote", filesystemManager.getRemoteAbsolutePath(dir, dirType));
        dirDetails.put("exists", Boolean.valueOf(dir.exists()));
        dirDetails.put("canRead", Boolean.valueOf(dir.canRead()));
        dirDetails.put("canWrite", Boolean.valueOf(dir.canWrite()));
        return dirDetails;
    }

    private boolean isExpectedInactivity() {
        // quicker check first
        ExecCommandResult result = LocalOs.execCommand("ps -a | grep restore_ | grep -v grep");
        if (!result.isSuccess()) {
            log.warn("Unable to grep for running restore processes due to : " + result.getError().toString());
        } else {
            String commOutputLog = Arrays.toString(result.getOutput().toArray());
            log.debug("grep for running restore processes : " + commOutputLog);
            for (String line : result.getOutput()) {
                if (line.contains("restore_")) {
                    log.info("There is a running restore process at the moment. Command output : " + commOutputLog);
                    return true;
                }
            }
        }
        // normal check
        JobEntity je = jobManager.getLast(JobType.FullRestore);
        if (je != null && je.getOperation() != null && je.getOperation().getStatus() == Status.ACTIVE) {
            log.info("There is a " + JobType.FullRestore + " running restore process at the moment.");
            return true;
        }
        log.debug("No active " + JobType.FullRestore + " restore processes at the moment.");
        je = jobManager.getLast(JobType.PitrRestore);
        if (je != null && je.getOperation() != null && je.getOperation().getStatus() == Status.ACTIVE) {
            log.info("There is a " + JobType.PitrRestore + " running restore process at the moment.");
            return true;
        }
        log.debug("No active " + JobType.PitrRestore + " restore processes at the moment.");
        return false;
    }
}
