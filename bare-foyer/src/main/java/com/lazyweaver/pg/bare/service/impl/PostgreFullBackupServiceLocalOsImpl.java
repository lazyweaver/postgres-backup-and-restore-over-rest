package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.FileType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.PostgreFullBackupService;
import org.springframework.stereotype.Service;

@Service
public class PostgreFullBackupServiceLocalOsImpl extends AbstractBackupOperations implements PostgreFullBackupService {

    @Override
    public JobEntity createBackup() {
        JobEntity unexpectedConditionEntity = getJobEntityUnexpectedCondition(getScriptFile().getName());
        if (unexpectedConditionEntity != null) {
            return unexpectedConditionEntity;
        }
        return createBackupJobAndExecute();
    }

    @Override
    protected String getCommand(File backupDir, File logsDir, JobEntity entity, String... params) {
        StringBuilder cmd = new StringBuilder();
        cmd.append(getScriptFile().getAbsolutePath());
        cmd.append(" \"").append(backupDir.getAbsolutePath()).append('"');
        cmd.append(" \"").append(jobManager.getStorage(entity)).append('"');
        cmd.append(" > ").append(filesystemManager.getFile(logsDir, FileType.StdOut).getAbsolutePath());
        cmd.append(" 2> ").append(filesystemManager.getFile(logsDir, FileType.StdErr).getAbsolutePath());
        cmd.append(" &");
        return cmd.toString();
    }

    @Override
    public JobType getJobType() {
        return JobType.FullBackup;
    }

    @Override
    protected DirType getDirType() {
        return DirType.BackupFull;
    }

    @Override
    protected FileType getEntityTypeScript() {
        return FileType.FullBackupScript;
    }
}
