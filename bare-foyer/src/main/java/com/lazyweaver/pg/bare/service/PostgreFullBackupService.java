package com.lazyweaver.pg.bare.service;

import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public interface PostgreFullBackupService extends JobProvidable {

    JobEntity createBackup();
}
