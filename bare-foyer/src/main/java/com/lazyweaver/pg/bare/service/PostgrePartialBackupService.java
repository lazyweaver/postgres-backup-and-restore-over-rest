package com.lazyweaver.pg.bare.service;

import java.util.List;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public interface PostgrePartialBackupService extends JobProvidable {

    List<JobEntity> getActiveJobs();

    JobEntity createBackup(String componentType, String componentName);
}
