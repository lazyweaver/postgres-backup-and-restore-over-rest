package com.lazyweaver.pg.bare.utils;

public interface ProgressLogger {

    public void log(String log);
}
