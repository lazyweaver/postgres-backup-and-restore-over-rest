package com.lazyweaver.pg.bare.service;

import com.lazyweaver.pg.bare.dto.OperationStatus;

public interface SystemService {

    OperationStatus getDatabaseStatus();

    OperationStatus getBareFoyerStatus();
}
