package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.FileType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.PostgreFullRestoreService;
import org.springframework.stereotype.Service;

@Service
public class PostgreFullRestoreServiceLocalOsImpl extends AbstractRestoreOperations implements PostgreFullRestoreService {

    @Override
    public JobEntity createRestore(String jobId) {
        return createRestoreJobAndExecute(jobId);
    }

    @Override
    protected JobType getJobType() {
        return JobType.FullRestore;
    }

    @Override
    protected FileType getEntityTypeScript() {
        return FileType.FullRestoreScript;
    }

    @Override
    protected JobType getSourceJobTypeForRestore() {
        return JobType.FullBackup;
    }

    @Override
    protected String getCommand(File archDir, File logsDir, JobEntity entity, String... params) {
        StringBuilder cmd = new StringBuilder();
        cmd.append(getScriptFile().getAbsolutePath());
        cmd.append(" \"").append(filesystemManager.getDirectory(DirType.PgData).getAbsolutePath()).append('"');
        cmd.append(" \"").append(archDir.getAbsolutePath()).append('"');
        cmd.append(" \"").append(filesystemManager.getDirectory(DirType.LogsRoot).getAbsolutePath()).append('"');
        cmd.append(" \"").append(jobManager.getStorage(entity)).append('"');
        cmd.append(" > ").append(filesystemManager.getFile(logsDir, FileType.StdOut).getAbsolutePath());
        cmd.append(" 2> ").append(filesystemManager.getFile(logsDir, FileType.StdErr).getAbsolutePath());
        cmd.append(" &");
        return cmd.toString();
    }
}
