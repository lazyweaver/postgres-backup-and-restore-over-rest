package com.lazyweaver.pg.bare.repository.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import com.lazyweaver.pg.bare.components.DateTime;
import com.lazyweaver.pg.bare.components.FilesystemManager;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.FileType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.dto.FileInfo;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.repository.JobFieldNames;
import com.lazyweaver.pg.bare.repository.JobRepository;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.repository.entity.UnknownJobEntity;
import com.lazyweaver.pg.bare.utils.IoUtils;
import com.lazyweaver.pg.bare.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobRepositoryLocalFilesImpl implements JobRepository {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private FilesystemManager filesystemManager;
    @Autowired
    private JobFieldNames jobFieldNames;
    @Autowired
    private DateTime dateTime;

    @Override
    public JobEntity getJobEntity(String jobId) {
        if (jobId == null || jobId.isEmpty()) {
            return new UnknownJobEntity("No job found for ID " + jobId);
        }
        File recordFile = new File(filesystemManager.getDirectory(DirType.JobsRoot), jobId);
        if (!recordFile.isFile()) {
            return new UnknownJobEntity(jobId, "No record found for job with ID \"" + jobId + "\"");
        }
        final Properties jobProps;
        try {
            jobProps = IoUtils.loadExternalProperties(recordFile);
        } catch (Exception e) {
            String msg = "Unable to load Properties for ID \"" + jobId + "\" from : " + recordFile.getAbsolutePath();
            log.error(msg);
            return new UnknownJobEntity(jobId, "Unable to load Properties for ID \"" + jobId + "\" from : " + recordFile.getAbsolutePath());
        }
        final JobEntity je = getExistingJobEntity(jobId, jobProps);
        addMissingDefaultData(je);
        addArchivesDataOrDefault(je);
        addErrorLogIfEligible(je);
        addRemoteDirectoriesIfEligible(je);
        return je;
    }

    @Override
    public String getStorage(JobEntity entity) {
        return getStorage(entity.getId());
    }

    @Override
    public String getStorage(String entityId) {
        if (entityId == null) {
            throw new IllegalArgumentException("entityId is null");
        }
        File recordFile = new File(filesystemManager.getDirectory(DirType.JobsRoot), entityId);
        return recordFile.getAbsolutePath();
    }

    @Override
    public void persist(JobEntity entity) {
        Objects.requireNonNull(entity, "Unable to create a Null JobEntity");
        Objects.requireNonNull(entity.getId(), "Unable to create a JobEntity with Null ID");
        File idFile = new File(filesystemManager.getDirectory(DirType.JobsRoot), entity.getId());
        boolean isSaved = IoUtils.saveToFile(createJobContent(entity), idFile);
        if (isSaved) {
            log.info("Created ID new file : " + idFile.getAbsolutePath());
        } else {
            log.error("Unable to create a new ID file : " + idFile.getAbsolutePath());
        }
    }

    @Override
    public void started(JobEntity entity) {
        File lastIdFile = getLastFile(entity.getJobType());
        if (IoUtils.saveToFile(entity.getId().getBytes(), lastIdFile)) {
            log.info("Set last job file : " + lastIdFile.getAbsolutePath());
        } else {
            log.error("Unable to set last job file : " + lastIdFile.getAbsolutePath());
        }
    }

    @Override
    public OperationStatus remove(JobEntity entity) {
        if (entity == null || entity.getId() == null || entity instanceof UnknownJobEntity) {
            log.info("Attempted to remove an non-existing job with id " + entity.getId());
            return new OperationStatus(Status.CMD_ERROR, entity.getOperation() != null ? entity.getOperation().getMessage() : "No job found");
        }
        if (entity.getOperation().getStatus() == Status.ACTIVE) {
            log.info("Attempted to remove an active job with id " + entity.getId());
            return new OperationStatus(Status.CMD_ERROR, "The job with ID \"" + entity.getId() + "\" is still active");
        }
        try {
            String path = (String) entity.getData().get(jobFieldNames.getLogs());
            removeHierarchyContentIfExists(entity.getId(), path);
            log.info("Removed all logs for job id " + entity.getId() + " under local path " + path);
        } catch (IOException e) {
            String msg = "Unable to delete hierarchical logs content due to : " + e.getMessage();
            log.error(msg, e);
            return new OperationStatus(Status.CMD_ERROR, msg);
        }
        if (entity.getJobType() == JobType.FullBackup || entity.getJobType() == JobType.PartialBackup) {
            String path = (String) entity.getData().get(jobFieldNames.getPath());
            try {
                removeHierarchyContentIfExists(entity.getId(), path);
                log.info("Removed all archives for job id " + entity.getId() + " under local path " + path);
            } catch (IOException e) {
                String msg = "Unable to delete hierarchical path content due to : " + e.getMessage();
                log.error(msg, e);
                return new OperationStatus(Status.CMD_ERROR, msg);
            }
        }
        File recordFile = new File(filesystemManager.getDirectory(DirType.JobsRoot), entity.getId());
        IoUtils.deleteFileIfExists(recordFile, false);
        // "last" should not be deleted for now
        log.info("Removed job and contents for id " + entity.getId() + " and type " + entity.getJobType());
        return new OperationStatus(Status.FINISHED, "Job and its contents removed for " + entity.getId());
    }

    private void removeHierarchyContentIfExists(String jobId, String path) throws IOException {
        File dir = new File(path);
        if (!IoUtils.deleteDirectoryContent(dir)) {
            throw new IOException("Unable to remove content of \"" + path + "\"");
        }
        if (!IoUtils.deleteDirectory(dir)) {
            log.warn("Unable to remove emptied directory \"" + path + "\"");
            return;
        }
        // clean only empty parent dirs - minute/hour/day/month
        for (int i = 0; i < 3; i++) {
            dir = dir.getParentFile();
            if (dir == null) {
                break;
            }
            if (StringUtils.toInt(dir.getName(), -1) < 0 || dir.list().length > 0) {
                break;
            }
            if (!IoUtils.deleteDirectory(dir)) {
                log.warn("Unable to remove empty directory \"" + path + "\"");
                return;
            }
            log.debug("Removed an empty hierarchical directory for job " + jobId);
        }
    }

    private JobEntity getExistingJobEntity(String jobId, Properties jobProps) {
        JobEntity je = new JobEntity(jobId, new OperationStatus(getStatusFromExecutionData(jobProps), getMessageFromExecutionData(jobProps))) {
            @Override
            public JobType getJobType() {
                return getJobTypeFromExecutionData(jobProps);
            }
        };
        jobProps.stringPropertyNames().stream().forEach(k -> je.getData().put(k, jobProps.getProperty(k)));
        return je;
    }

    private void addArchivesDataOrDefault(JobEntity js) {
        if (!"success".equals(js.getData().get(jobFieldNames.getEndStatus()))) {
            return;
        }
        final File archDir = new File(js.getData().get(jobFieldNames.getPath()).toString());
        if (!archDir.isDirectory()) {
            applyError(js, "Expected directory does not exist under path " + archDir.getAbsolutePath());
            return;
        }
        final List<FileInfo> archFileInfoFiles = new ArrayList<>();
        Arrays.stream(archDir.listFiles()).forEach(f -> {
            try {
                archFileInfoFiles.add(new FileInfo(getQuickFileType(f), filesystemManager.getRemoteAbsolutePath(f, DirType.BackupRoot), Files.size(f.toPath())));
            } catch (IOException e) {
                log.error("From job \"" + js.getId() + "\" unable to get file size of \"" + f.getAbsolutePath() + "\" due to : " + e.getMessage());
                archFileInfoFiles.add(new FileInfo(getQuickFileType(f), f.getAbsolutePath(), -1));
            }
        });
        if (archFileInfoFiles.isEmpty()) {
            applyError(js, "Expected directory exists but contains no files " + archDir.getAbsolutePath());
            return;
        }
        js.getData().put(jobFieldNames.getArchive(), archFileInfoFiles);
    }

    private void addErrorLogIfEligible(JobEntity js) {
        if (!"error".equals(js.getData().get(jobFieldNames.getEndStatus()))) {
            return;
        }
        Object logDir = js.getData().get(jobFieldNames.getLogs());
        if (logDir == null) {
            log.error("For job \"" + js.getId() + "\" missing field in record \"" + jobFieldNames.getEndStatus() + "\"");
            return;
        }
        final File stdErr = filesystemManager.getFile(new File(logDir.toString()), FileType.StdErr);
        long stdErrSize = 0;
        try {
            stdErrSize = Files.size(stdErr.toPath());
        } catch (Exception e) {
            log.error("For job \"" + js.getId() + "\" unable to read stderr file \"" + stdErr.getAbsolutePath() + "\" due to : " + e.getMessage(), e);
            return;
        }
        if (stdErrSize < 2048) {
            byte[] stdErrContent = IoUtils.readAllBytes(stdErr);
            if (stdErrContent != null) {
                js.getData().put(jobFieldNames.getExecutionErrorLog(), new String(stdErrContent));
            }
        } else {
            js.getData().put(jobFieldNames.getExecutionErrorLog(), "Stderr file (" + stdErr.getAbsolutePath() + ") is too big to return content (" + stdErrSize + " bytes)");
        }
    }

    private void addRemoteDirectoriesIfEligible(JobEntity je) {
        if (je.getData().containsKey(jobFieldNames.getPath())) {
            je.getData().put(jobFieldNames.getPathRemote(), filesystemManager.getRemoteAbsolutePath(new File((String) je.getData().get(jobFieldNames.getPath())), DirType.BackupRoot));
        }
        if (je.getData().containsKey(jobFieldNames.getLogs())) {
            je.getData().put(jobFieldNames.getLogsRemote(), filesystemManager.getRemoteAbsolutePath(new File((String) je.getData().get(jobFieldNames.getLogs())), DirType.LogsRoot));
        }
    }

    private void applyError(JobEntity js, String errorMessage) {
        if ("success".equals(js.getData().get(jobFieldNames.getEndStatus()))) {
            log.error("Success execution job \"" + js.getId() + "\" but has a runtime error : " + errorMessage);
            js.getData().put(jobFieldNames.getEndStatus(), "error");
        }
        js.getData().put(jobFieldNames.getEndMessage(), js.getData().getOrDefault(jobFieldNames.getEndMessage(), "") + " " + errorMessage);
        js.getData().put(jobFieldNames.getArchive(), "");
    }

    private void addMissingDefaultData(JobEntity js) {
        if (!js.getData().containsKey(jobFieldNames.getEndStatus())) {
            js.getData().put(jobFieldNames.getEndStatus(), "");
        }
        if (!js.getData().containsKey(jobFieldNames.getEndMessage())) {
            js.getData().put(jobFieldNames.getEndMessage(), "");
        }
        if (!js.getData().containsKey(jobFieldNames.getDateEnded())) {
            js.getData().put(jobFieldNames.getDateEnded(), "");
        }
    }

    private Status getStatusFromExecutionData(Properties jobProps) {
        String execStatus = jobProps.getProperty(jobFieldNames.getEndStatus(), "running");
        if (execStatus.equals("success") || execStatus.equals("error")) {
            return Status.FINISHED;
        }
        return jobProps.containsKey(jobFieldNames.getDateEnded()) ? Status.INACTIVE : Status.ACTIVE;
    }

    private String getMessageFromExecutionData(Properties jobProps) {
        String execMessage = jobProps.getProperty(jobFieldNames.getEndMessage());
        return execMessage;
    }

    private JobType getJobTypeFromExecutionData(Properties jobProps) {
        JobType jobType = null;
        String propValue = null;
        try {
            propValue = jobProps.getProperty(jobFieldNames.getType());
            jobType = JobType.valueOf(propValue);
        } catch (Exception e) {
            log.error("Unable to get JobType enum from data \"" + propValue + "\" due to : " + e.getMessage(), e);
        }
        return jobType;
    }

    private byte[] createJobContent(JobEntity entity) {
        StringBuilder bld = new StringBuilder();
        bld.append(jobFieldNames.getRequestParams()).append('=').append(entity.getData().get(jobFieldNames.getRequestParams())).append("\n");
        bld.append(jobFieldNames.getPath()).append('=').append(entity.getData().get(jobFieldNames.getPath()).toString().replace("\\", "\\\\")).append("\n");
        bld.append(jobFieldNames.getLogs()).append('=').append(entity.getData().get(jobFieldNames.getLogs()).toString().replace("\\", "\\\\")).append("\n");
        bld.append(jobFieldNames.getType()).append('=').append(entity.getJobType().name()).append("\n");
        bld.append(jobFieldNames.getDatePattern()).append('=').append(dateTime.getFormatPattern()).append("\n");
        bld.append(jobFieldNames.getDateStarted()).append('=').append(entity.getData().get(jobFieldNames.getDateStarted())).append("\n");
        return bld.toString().getBytes();
    }

    @Override
    public JobEntity getLast(JobType jobType) {
        File f = getLastFile(jobType);
        if (!f.isFile()) {
            return null;
        }
        byte[] jobIdBytes = IoUtils.readAllBytes(f);
        if (jobIdBytes == null || jobIdBytes.length < 1) {
            log.error("Empty file content of " + f.getAbsolutePath());
            return null;
        }
        return getJobEntity(new String(jobIdBytes));
    }

    private File getLastFile(JobType jobType) {
        return new File(filesystemManager.getDirectory(DirType.JobsRoot), jobType.name() + "-last");
    }

    private static String getQuickFileType(File f) {
        if (f == null) {
            return null;
        }
        String type = f.getName();
        int dotIdx = type.indexOf('.');
        if (dotIdx > 1) {
            type = type.substring(0, dotIdx);
        }
        return type;
    }
}
