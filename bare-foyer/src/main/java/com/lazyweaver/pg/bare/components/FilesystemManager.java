package com.lazyweaver.pg.bare.components;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public interface FilesystemManager {

    public enum DirType {
        BackupRoot, BackupFull, BackupPartial, LogsRoot, LogsArchiving, JobsRoot, PgData, PgTools
    }

    public enum JobType {
        FullBackup, PartialBackup, PartialRestore, FullRestore, PitrRestore
    }

    public enum FileType {
        StdErr, StdOut, FullBackupScript, FullRestoreScript, PartialBackupScript, PartialRestoreScript, PitrRestoreScript
    }

    File getDirectory(DirType dirType);

    File getFile(File parentDir, FileType entityType);

    File createDirectoryStructure(DirType parent, String directoryRelativePath) throws IOException;

    String getDirectoryRelativePath(LocalDateTime localDateTime);

    String getRemoteAbsolutePath(File localFile, DirType dirType);
}
