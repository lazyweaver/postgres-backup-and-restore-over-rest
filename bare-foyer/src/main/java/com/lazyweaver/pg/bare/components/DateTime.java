package com.lazyweaver.pg.bare.components;

import java.time.LocalDateTime;

public interface DateTime {

    LocalDateTime now();

    String getFormatPattern();

    String format(LocalDateTime localDateTime);

    String format(LocalDateTime localDateTime, String pattern);

    LocalDateTime parse(String dateTime);
}
