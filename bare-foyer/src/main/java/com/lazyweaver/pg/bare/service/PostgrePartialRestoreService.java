package com.lazyweaver.pg.bare.service;

import java.util.List;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public interface PostgrePartialRestoreService extends JobProvidable, JobRestoreCreatable {

    List<JobEntity> getActiveJobs();
}
