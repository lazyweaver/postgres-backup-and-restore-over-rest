package com.lazyweaver.pg.bare.controllers;

import com.lazyweaver.pg.bare.dto.JobEntityList;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.PostgreFullRestoreService;
import com.lazyweaver.pg.bare.service.PostgrePartialRestoreService;
import com.lazyweaver.pg.bare.service.PostgrePitrRestoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/restore")
@Api(tags = "Restore")
public class RestoreController {

    @Autowired
    private PostgreFullRestoreService fullRestoreService;
    @Autowired
    private PostgrePartialRestoreService partialRestoreService;
    @Autowired
    private PostgrePitrRestoreService pitrRestoreService;

    @GetMapping(value = "/full")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of the Last \"Full Restore\" job")
    public ResponseEntity<JobEntity> getLastFullRestoreJob() {
        return ResponseEntity.ok(fullRestoreService.getJobEntity());
    }

    @GetMapping(value = "/full/{jobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of a \"Full Restore\" job by ID")
    public ResponseEntity<JobEntity> getFullRestoreJob(@ApiParam("Job ID") @PathVariable("jobId") String jobId) {
        return ResponseEntity.ok(fullRestoreService.getJobEntity(jobId));
    }

    @PostMapping(value = "/full/{sourceJobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Creates a new \"Full Restore\" job using a \"Full Backup\" job ID", notes = "On success an entity with a generated ID is returned and status ACTIVE")
    public ResponseEntity<JobEntity> createFullRestore(@ApiParam("Source \"Full Backup\" job ID") @PathVariable("sourceJobId") String sourceJobId) {
        return ResponseEntity.ok(fullRestoreService.createRestore(sourceJobId));
    }

    @GetMapping(value = "/pitr")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of the Last \"Point-in-Time Recovery\" job")
    public ResponseEntity<JobEntity> getLastPointInTimeRestoreJob() {
        return ResponseEntity.ok(pitrRestoreService.getJobEntity());
    }

    @GetMapping(value = "/pitr/{jobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of a \"Point-in-Time Recovery\" job by ID")
    public ResponseEntity<JobEntity> getPointInTimeRestoreJob(@ApiParam("Job ID") @PathVariable("jobId") String jobId) {
        return ResponseEntity.ok(pitrRestoreService.getJobEntity(jobId));
    }

    @PostMapping(value = "/pitr/{recoveryDateTime}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Creates a new \"Point-in-Time Recovery\" job for the requested date \"dd-MM-yyyy HH:mm\"", notes = "On success an entity with a generated ID is returned and status ACTIVE")
    public ResponseEntity<JobEntity> createPointInTimeRestore(@ApiParam("Date in format \"dd-MM-yyyy HH:mm\"") @PathVariable("recoveryDateTime") String recoveryDateTime) {
        return ResponseEntity.ok(pitrRestoreService.createRestore(recoveryDateTime));
    }

    @GetMapping(value = "/partial/active")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Gets all active \"Partial Restore\" jobs")
    public ResponseEntity<JobEntityList> getPartialActiveJobs() {
        return ResponseEntity.ok(new JobEntityList(partialRestoreService.getActiveJobs()));
    }

    @GetMapping(value = "/partial")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of the Last \"Partial Restore\" job")
    public ResponseEntity<JobEntity> getLastPartialRestoreJob() {
        return ResponseEntity.ok(partialRestoreService.getJobEntity());
    }

    @GetMapping(value = "/partial/{jobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Provides information of a \"Partial Restore\" job by ID")
    public ResponseEntity<JobEntity> getPartialRestoreJob(@ApiParam("Job ID") @PathVariable("jobId") String jobId) {
        return ResponseEntity.ok(partialRestoreService.getJobEntity(jobId));
    }

    @PostMapping(value = "/partial/{sourceJobId}")
    @ApiImplicitParam(name = "api-key", value = "Secret Key (Header)", required = true, allowEmptyValue = false, paramType = "header", dataTypeClass = String.class, example = "developerTest123")
    @ApiOperation(value = "Creates a new \"Partial Restore\" job using a \"Partial Backup\" job ID", notes = "On success an entity with a generated ID is returned and status ACTIVE")
    public ResponseEntity<JobEntity> createPartialRestore(@ApiParam("Source \"Partial Backup\" job ID") @PathVariable("sourceJobId") String sourceJobId) {
        return ResponseEntity.ok(partialRestoreService.createRestore(sourceJobId));
    }
}
