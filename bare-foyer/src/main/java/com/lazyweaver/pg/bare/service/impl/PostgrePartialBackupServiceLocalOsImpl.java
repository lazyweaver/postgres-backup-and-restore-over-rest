package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.components.FilesystemManager.FileType;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.PostgrePartialBackupService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PostgrePartialBackupServiceLocalOsImpl extends AbstractBackupOperations implements PostgrePartialBackupService {

    @Value("${postgres.backup.db.main}")
    private String mainDatabaseName;
    @Value("${postgres.backup.db.table.pattern}")
    private String nameTablePattern;
    @Value("${postgres.backup.db.schema.pattern}")
    private String nameSchemaPattern;

    @Override
    public List<JobEntity> getActiveJobs() {
        return getActiveJobsByScriptName();
    }

    @Override
    public JobEntity createBackup(String componentType, String componentName) {
        JobEntity unexpectedConditionEntity = getJobEntityUnexpectedCondition("backup_partial");
        if (unexpectedConditionEntity != null) {
            return unexpectedConditionEntity;
        }
        unexpectedConditionEntity = getJobEntityUnexpectedCondition("psql");
        if (unexpectedConditionEntity != null) {
            return unexpectedConditionEntity;
        }
        // input validation
        try {
            getPgDumpParameter(componentType);
        } catch (Exception e) {
            log.error("Error getting valid pg_dump parameter : " + e.getMessage(), e);
            return getJobEntity(Status.CMD_ERROR, e.getMessage());
        }
        try {
            validateComponentName(componentType, componentName);
        } catch (Exception e) {
            log.info("Unable to validate \"" + componentName + "\" of type \"" + componentType + "\" due to : " + e.getMessage());
            return getJobEntity(Status.CMD_ERROR, e.getMessage());
        }
        componentName = prepareQuotedValue(componentName);
        return createBackupJobAndExecute(componentType, componentName);
    }

    @Override
    protected String getCommand(File backupDir, File logsDir, JobEntity entity, String... params) {
        if (params == null || params.length < 2) {
            throw new IllegalArgumentException("Expected at least 2 parameters");
        }
        StringBuilder cmd = new StringBuilder();
        cmd.append(getScriptFile().getAbsolutePath());
        cmd.append(" \"").append(mainDatabaseName).append('"');
        cmd.append(" \"").append(getPgDumpParameter(params[0])).append('"');
        cmd.append(" \"").append(escapeCommandParameter(params[1])).append('"');
        cmd.append(" \"").append(backupDir.getAbsolutePath()).append('"');
        cmd.append(" \"").append(jobManager.getStorage(entity)).append('"');
        cmd.append(" > ").append(filesystemManager.getFile(logsDir, FileType.StdOut).getAbsolutePath());
        cmd.append(" 2> ").append(filesystemManager.getFile(logsDir, FileType.StdErr).getAbsolutePath());
        cmd.append(" &");
        return cmd.toString();
    }

    @Override
    public JobType getJobType() {
        return JobType.PartialBackup;
    }

    @Override
    protected DirType getDirType() {
        return DirType.BackupPartial;
    }

    @Override
    protected FileType getEntityTypeScript() {
        return FileType.PartialBackupScript;
    }

    private static Character getPgDumpParameter(String componentType) {
        if ("schema".equals(componentType)) {
            return Character.valueOf('n');
        } else if ("table".equals(componentType)) {
            return Character.valueOf('t');
        }
        throw new IllegalArgumentException("Unsupported componentType \"" + componentType + "\". Expected one of : \"schema\", \"table\"");
    }

    private String prepareQuotedValue(String componentName) {
        if (componentName == null) {
            return null;
        }
        componentName = componentName.replace("\"", "");
        final int idxSchemaSeparat = componentName.indexOf(".");
        if (idxSchemaSeparat > 0) {
            componentName = componentName.substring(0, idxSchemaSeparat) + "\".\"" + componentName.substring(idxSchemaSeparat + 1);
        }
        return "\"" + componentName + "\"";
    }

    private void validateComponentName(String componentType, String componentName) {
        Pattern pattern = getPattern(componentType);
        Matcher matcher = pattern.matcher(componentName);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Component name \"" + componentName + "\" does not match " + componentType + " pattern \"" + pattern + "\"");
        }
    }

    private Pattern patternDbTableName = null;
    private Pattern patternSchemaName = null;

    private Pattern getPattern(String componentType) {
        if ("table".equals(componentType)) {
            if (patternDbTableName == null) {
                patternDbTableName = Pattern.compile(nameTablePattern);
            }
            return patternDbTableName;
        }
        if ("schema".equals(componentType)) {
            if (patternSchemaName == null) {
                patternSchemaName = Pattern.compile(nameSchemaPattern);
            }
            return patternSchemaName;
        }
        throw new IllegalArgumentException("Unsupported pattern for componentType \"" + componentType + "\". Expected one of : \"schema\", \"table\"");
    }
}
