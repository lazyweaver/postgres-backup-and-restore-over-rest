package com.lazyweaver.pg.bare.service;

import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public interface PostgrePitrRestoreService extends JobProvidable, JobRestoreCreatable {

    JobEntity createRestore(String toDateString);
}
