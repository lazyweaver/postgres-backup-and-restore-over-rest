package com.lazyweaver.pg.bare.utils;

import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class ThreadUtils {

    private static final Logger log = LoggerFactory.getLogger(ThreadUtils.class);

    private ThreadUtils() {}

    public static void setThreadNamePreserveCounter(Thread thread, String newName) {
        if (thread == null || newName == null) {
            return;
        }
        String currName = thread.getName().replace("Thread", "");
        thread.setName(newName + currName);
    }

    public static boolean sleepSeconds(long seconds) {
        try {
            log.trace("Will sleep thread for " + seconds + " seconds");
            TimeUnit.SECONDS.sleep(seconds);
        } catch (InterruptedException e) {
            log.error("Unable to sleep thread for " + seconds + " seconds");
            return false;
        }
        return true;
    }

    public static boolean sleepMiliseconds(long miliseconds) {
        try {
            log.trace("Will sleep thread for " + miliseconds + " miliseconds");
            TimeUnit.MILLISECONDS.sleep(miliseconds);
        } catch (InterruptedException e) {
            log.error("Unable to sleep thread for " + miliseconds + " miliseconds");
            return false;
        }
        return true;
    }
}
