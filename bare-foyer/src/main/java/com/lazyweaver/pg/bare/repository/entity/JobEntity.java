package com.lazyweaver.pg.bare.repository.entity;

import java.util.Map;
import java.util.TreeMap;
import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.OperationStatus;

public abstract class JobEntity {

    private String id;
    private final OperationStatus operation;
    private final Map<String, Object> data = new TreeMap<>();

    public JobEntity() {
        this(null);
    }

    public JobEntity(OperationStatus operationStatus) {
        this(null, operationStatus);
    }

    public JobEntity(String jobId, OperationStatus operationStatus) {
        this.id = jobId;
        this.operation = (operationStatus == null) ? new OperationStatus() : operationStatus;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public OperationStatus getOperation() {
        return operation;
    }

    public abstract JobType getJobType();

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [id=" + id + ", data=" + data + ", operation=" + operation + "]";
    }
}
