package com.lazyweaver.pg.bare.service.impl;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Arrays;
import com.lazyweaver.pg.bare.components.FilesystemManager.DirType;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public abstract class AbstractBackupOperations extends AbstractJobOperations {

    protected final JobEntity createBackupJobAndExecute(String... params) {
        final LocalDateTime now = dateTime.now();
        File newBackupDirObj;
        File newLogsDirObj;
        try {
            newBackupDirObj = filesystemManager.createDirectoryStructure(getDirType(), filesystemManager.getDirectoryRelativePath(now));
            newLogsDirObj = filesystemManager.createDirectoryStructure(DirType.LogsArchiving, filesystemManager.getDirectoryRelativePath(now));
        } catch (Exception e) {
            log.error("Unable to create Backup or Logs directory structure", e);
            return getJobEntity(Status.CMD_ERROR, e.getMessage());
        }
        JobEntity je = createJobEntity(now, newBackupDirObj, newLogsDirObj);
        je.getData().put(jobFieldNames.getRequestParams(), Arrays.toString(params));
        jobManager.persist(je);
        log.info("New backup job persisted " + je.getId());
        executeCommand(getCommand(newBackupDirObj, newLogsDirObj, je, params), je);
        return je;
    }

    protected String escapeCommandParameter(String param) {
        if (param == null) {
            return null;
        }
        return param.replace("\"", "\\\"");
    }

    protected abstract DirType getDirType();
    protected abstract String getCommand(File backupDir, File logsDir, JobEntity entity, String... params);
}
