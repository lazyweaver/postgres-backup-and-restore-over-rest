package com.lazyweaver.pg.bare.components.impl;

import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.components.JobManager;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.repository.JobRepository;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.repository.entity.UnknownJobEntity;
import org.springframework.stereotype.Service;

@Service
public class JobManagerImpl implements JobManager {

    private final JobRepository jobRepository;

    public JobManagerImpl(JobRepository jobRepository) {
        this.jobRepository = jobRepository;
    }

    @Override
    public JobEntity getJobEntity(String jobId) {
        return jobRepository.getJobEntity(jobId);
    }

    @Override
    public void persist(JobEntity jobEntity) {
        jobRepository.persist(jobEntity);
    }

    @Override
    public String getStorage(JobEntity jobEntity) {
        return jobRepository.getStorage(jobEntity);
    }

    @Override
    public String getStorage(String entityId) {
        return jobRepository.getStorage(entityId);
    }

    @Override
    public OperationStatus remove(String jobId) {
        JobEntity existingJob = getJobEntity(jobId);
        if (existingJob instanceof UnknownJobEntity) {
            return new OperationStatus(Status.CMD_ERROR, existingJob.getOperation().getMessage());
        }
        return remove(getJobEntity(jobId));
    }

    @Override
    public OperationStatus remove(JobEntity jobEntity) {
        return jobRepository.remove(jobEntity);
    }

    @Override
    public void started(JobEntity jobEntity) {
        jobRepository.started(jobEntity);
    }

    @Override
    public JobEntity getLast(JobType jobType) {
        return jobRepository.getLast(jobType);
    }
}
