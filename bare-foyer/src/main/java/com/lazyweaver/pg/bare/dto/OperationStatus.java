package com.lazyweaver.pg.bare.dto;

public class OperationStatus extends AbstractStatus {

    private String message;

    public OperationStatus() {
        this(Status.UNKNOWN);
    }

    public OperationStatus(Status status) {
        this(status, null);
    }

    public OperationStatus(Status status, String message) {
        super(status);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " [message=" + message + ", status=" + getStatus() + "]";
    }
}
