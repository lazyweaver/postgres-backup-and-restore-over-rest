package com.lazyweaver.pg.bare.repository;

import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;

public interface JobRepository {

    void persist(JobEntity entity);

    OperationStatus remove(JobEntity jobEntity);

    void started(JobEntity jobEntity);
    
    String getStorage(JobEntity jobEntity);
    
    String getStorage(String jobEntityId);

    JobEntity getJobEntity(String jobId);
    
    JobEntity getLast(JobType jobType);
}
