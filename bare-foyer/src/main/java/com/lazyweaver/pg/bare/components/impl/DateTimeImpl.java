package com.lazyweaver.pg.bare.components.impl;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import com.lazyweaver.pg.bare.components.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DateTimeImpl implements DateTime {

    @Value("${postgres.backup.date.format}")
    private String dateFormatPattern;
    @Value("${postgres.backup.date.zoneid}")
    private String timeZoneId;

    private Map<String, DateTimeFormatter> dateTimeFormatters = new HashMap<>();

    @Override
    public LocalDateTime now() {
        return LocalDateTime.now(ZoneId.of(timeZoneId));
    }

    @Override
    public String getFormatPattern() {
        return dateFormatPattern;
    }

    @Override
    public String format(LocalDateTime localDateTime) {
        return format(localDateTime, dateFormatPattern);
    }

    @Override
    public String format(LocalDateTime localDateTime, String pattern) {
        if (localDateTime == null) {
            return null;
        }
        return getDateTimeFormatter(pattern).format(localDateTime);
    }

    @Override
    public LocalDateTime parse(String dateTime) {
        if (dateTime == null || dateTime.isBlank()) {
            throw new DateTimeException("null or empty string can not be converted to " + LocalDateTime.class.getSimpleName());
        }
        return LocalDateTime.parse(dateTime, getDateTimeFormatter(dateFormatPattern));
    }

    private DateTimeFormatter getDateTimeFormatter(String pattern) {
        DateTimeFormatter dtf = dateTimeFormatters.get(pattern);
        if (dtf == null) {
            dtf = DateTimeFormatter.ofPattern(pattern);
            dateTimeFormatters.put(pattern, dtf);
        }
        return dtf;
    }
}
