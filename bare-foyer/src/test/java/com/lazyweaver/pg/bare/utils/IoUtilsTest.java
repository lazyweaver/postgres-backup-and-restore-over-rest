package com.lazyweaver.pg.bare.utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import java.util.zip.ZipInputStream;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class IoUtilsTest {

    private static final String copyTestFromPath = "src/test/resources/com/ioCopyTest/test.properties";

    @BeforeClass
    public static void initAll() {
        deleteFileIfExistsFailOnError(getCopyTestBackupFilePath(), true);
    }

    @AfterClass
    public static void cleanAll() {
        deleteFileIfExistsFailOnError(getCopyTestBackupFilePath(), false);
    }

    @Test
    public void saveToFile() {
        Assert.assertFalse(IoUtils.saveToFile("abc".getBytes(), null));
        Assert.assertFalse(IoUtils.saveToFile(null, new File("test")));
        File testFile = new File("test");
        if (testFile.exists()) {
            Assert.assertTrue(testFile.delete());
        }
        Assert.assertTrue(IoUtils.saveToFile("abc".getBytes(), testFile));
        Assert.assertTrue(testFile.isFile());
        Assert.assertEquals("abc", new String(IoUtils.readAllBytes(testFile)));
        Assert.assertTrue(IoUtils.saveToFile("abc".getBytes(), testFile));
        Assert.assertTrue(testFile.isFile());
        Assert.assertEquals("abc", new String(IoUtils.readAllBytes(testFile)));
        Assert.assertTrue(IoUtils.saveToFile("abc".getBytes(), testFile));
        Assert.assertTrue(testFile.isFile());
        Assert.assertEquals("abc", new String(IoUtils.readAllBytes(testFile)));
        Assert.assertTrue(testFile.delete());// the file should be deletable
    }

    @Test
    public void toByteArraySortedKeys() {
        try {
            IoUtils.toByteArraySortedKeys(null);
            Assert.fail("Exception expected");
        } catch (NullPointerException npe) {
            Assert.assertEquals("Map parameter is mandatory", npe.getMessage());
        }
        byte[] result = null;
        Properties props = new Properties();
        //
        result = IoUtils.toByteArraySortedKeys(props);
        Assert.assertNotNull(result);
        Assert.assertEquals("", new String(result));
        //
        props.put("cda", "cdaVal");
        props.put("abc", "abcVal");
        props.put("ggg", "gggVal");
        props.put("ppp", "pppVal");
        props.put("bca", "bcaVal");
        result = IoUtils.toByteArraySortedKeys(props);
        Assert.assertNotNull(result);
        String NL = System.getProperty("line.separator");
        Assert.assertEquals("abc=abcVal" + NL + "bca=bcaVal" + NL + "cda=cdaVal" + NL + "ggg=gggVal" + NL + "ppp=pppVal" + NL, new String(result));
    }

    @Test
    public void copyFileAsFiles() {
        Assert.assertTrue(new File(copyTestFromPath).isFile());
        Assert.assertFalse(IoUtils.copyFile((String) null, (String) null));
        Assert.assertFalse(IoUtils.copyFile((File) null, (File) null));
        Assert.assertFalse(IoUtils.copyFile(copyTestFromPath, (String) null));
        Assert.assertFalse(IoUtils.copyFile(new File(copyTestFromPath), (File) null));
        Assert.assertFalse(IoUtils.copyFile(copyTestFromPath + "NoSuch", getCopyTestBackupFilePath().getAbsolutePath()));
        Assert.assertFalse(IoUtils.copyFile(new File(copyTestFromPath + "NoSuch"), getCopyTestBackupFilePath()));
        Assert.assertTrue(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath()));
        Assert.assertTrue(getCopyTestBackupFilePath().isFile());
        // do not overwrite
        Assert.assertFalse(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath()));
        // overwrite
        Assert.assertTrue(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath(), true));
        // overwrite again
        Assert.assertTrue(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath(), true));
    }

    @Test
    public void copyFileAsDirectories() {
        File dirTest = new File(copyTestFromPath).getParentFile();
        File targetTest = new File(dirTest.getParentFile(), dirTest.getName() + ".del");
        Assert.assertTrue(dirTest.isDirectory());
        Assert.assertFalse(targetTest.isDirectory());

        Assert.assertTrue(IoUtils.copyFile(dirTest, targetTest));
        Assert.assertTrue(targetTest.isDirectory());
        // do not overwrite
        Assert.assertFalse(IoUtils.copyFile(dirTest, targetTest));
        // overwrite
        Assert.assertTrue(IoUtils.copyFile(dirTest, targetTest, true));
        // overwrite again
        Assert.assertTrue(IoUtils.copyFile(dirTest, targetTest, true));
        // clean stuff
        Assert.assertTrue(IoUtils.deleteDirectory(targetTest));
    }

    @Test
    public void createDirectories() {
        // bad input - null
        ActionResult result = IoUtils.createDirectories(null);
        Assert.assertFalse(result.isSuccess());
        // bad input - file
        Assert.assertTrue(new File(copyTestFromPath).isFile());
        result = IoUtils.createDirectories(new File(copyTestFromPath));
        Assert.assertFalse(result.isSuccess());
        // New dir
        File newDir = new File(new File(copyTestFromPath).getParentFile(), Long.toString(new Date().getTime()));
        result = IoUtils.createDirectories(newDir);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(newDir.isDirectory());
        // again
        result = IoUtils.createDirectories(newDir);
        Assert.assertTrue(result.isSuccess());
        // attempt to create when parent dir is missing
        Assert.assertTrue(IoUtils.deleteDirectory(newDir));
        File newNestedDir = new File(newDir, Long.toString(new Date().getTime()));
        Assert.assertFalse(newNestedDir.getParentFile().exists());
        result = IoUtils.createDirectories(newNestedDir);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(newNestedDir.isDirectory());
        // clean
        Assert.assertTrue(IoUtils.deleteDirectory(newNestedDir));
    }

    @Test
    public void createDirectory() {
        // bad input - null
        ActionResult result = IoUtils.createDirectory(null);
        Assert.assertFalse(result.isSuccess());
        // bad input - file
        Assert.assertTrue(new File(copyTestFromPath).isFile());
        result = IoUtils.createDirectory(new File(copyTestFromPath));
        Assert.assertFalse(result.isSuccess());
        // New dir
        File newDir = new File(new File(copyTestFromPath).getParentFile(), Long.toString(new Date().getTime()));
        result = IoUtils.createDirectory(newDir);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(newDir.isDirectory());
        // again
        result = IoUtils.createDirectory(newDir);
        Assert.assertTrue(result.isSuccess());
        // attempt to create when parent dir is missing
        Assert.assertTrue(IoUtils.deleteDirectory(newDir));
        File newNestedDir = new File(newDir, Long.toString(new Date().getTime()));
        Assert.assertFalse(newNestedDir.getParentFile().exists());
        result = IoUtils.createDirectory(newNestedDir);
        Assert.assertFalse(result.isSuccess());
        Assert.assertEquals("java.nio.file.NoSuchFileException:" + newNestedDir.getPath(), result.getErrorMessage());
        Assert.assertFalse(newNestedDir.isDirectory());
    }

    @Test
    public void compressAsZip() {
        final File zipFileToValidate = new File("compressAsZip-testcase.zip");
        final File zipDirToExtract = new File("compressAsZip-testcase");
        compressAsZipTestClean(zipDirToExtract, zipFileToValidate);

        byte[] zipContent = IoUtils.compressAsZip("test text".getBytes(), "zipEntryName.txt");
        Assert.assertNotNull(zipContent);
        Assert.assertTrue(zipContent.length > 0);

        Assert.assertTrue("Unable to save ZIP " + zipFileToValidate.getAbsolutePath(), IoUtils.saveToFile(zipContent, zipFileToValidate));
        try {
            IoUtils.unzipAll(zipFileToValidate.getAbsolutePath(), true, zipDirToExtract);
        } catch (IOException e) {
            Assert.fail("Unable to extract ZIP to dir due to :" + e.getMessage());
        }
        File validateExtractedFile = new File(zipDirToExtract, "zipEntryName.txt");
        Assert.assertTrue("Expected a file " + validateExtractedFile.getAbsolutePath(), validateExtractedFile.isFile());
        byte[] maybeOriginalData = IoUtils.readAllBytes(validateExtractedFile);
        Assert.assertEquals("test text", new String(maybeOriginalData));
        compressAsZipTestClean(zipDirToExtract, zipFileToValidate, validateExtractedFile);
    }

    private static void compressAsZipTestClean(File dir, File ...files) {
        if (dir.isDirectory()) {
            Assert.assertTrue("Unable to delete " + dir.getAbsolutePath(), IoUtils.deleteDirectory(dir));
        }
        for(File file : files) {
            if (file.isFile()) {
                Assert.assertTrue("Unable to delete " + file.getAbsolutePath(), file.delete());
            }
        }
    }
    
    @Test
    public void unzipAll() {
        File destDir = new File("ioCopyTest/noSuch/noSuchAgain");
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, "Unable to create the non-existing directory " + destDir.getAbsolutePath());
        destDir = new File("src/test/resources/com/ioZipTest/test_config.zip");
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, "Not a directory " + destDir.getAbsolutePath());
        destDir = new File("ioCopyTest");
        Assert.assertFalse(destDir.exists());
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, null);
        assertUnzippedContent(destDir);
        Assert.assertTrue(IoUtils.deleteDirectoryContent(destDir));
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, null);
        assertUnzippedContent(destDir);
        Assert.assertTrue(IoUtils.deleteDirectoryContent(destDir));
        Assert.assertTrue(IoUtils.saveToFile("abc".getBytes(), new File(destDir, "a.txt")));
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, "The destination directory \"" + destDir.getAbsolutePath() + "\" is not empty");
        Assert.assertTrue(IoUtils.deleteDirectory(destDir));
    }

    private static void unzipAllFromFileToFile(File source, File dest, String expectedException) {
        ZipInputStream zis = null;
        try {
            zis = IoUtils.getZipInputStream(source);
            IoUtils.unzipAll(zis, dest);
            if (expectedException != null) {
                Assert.fail("Exception expected");
            }
        } catch (IOException e) {
            if (!e.getMessage().equals(expectedException)) {
                e.printStackTrace();
                Assert.fail(e.getMessage());
            }
        } finally {
            IoUtils.close(zis);
        }
    }

    private static void assertUnzippedContent(File fromDir) {
        assertFileExistsAndContent(true, new File(fromDir, "core-site.xml"), "<name>fs.defaultFS</name>");
        assertFileExistsAndContent(true, new File(fromDir, "hadoop-env.sh"), "export JAVA_HOME");
        assertFileExistsAndContent(true, new File(fromDir, "hdfs-site.xml"), "<name>dfs.namenode.name.dir</name>");
        assertFileExistsAndContent(true, new File(fromDir, "mapred-site.xml"), "<name>mapreduce.framework.name</name>");
        assertFileExistsAndContent(true, new File(fromDir, "slaves"), "slaveFour");
        assertFileExistsAndContent(true, new File(fromDir, "yarn-site.xml"), "<name>yarn.nodemanager.aux-services</name>");
        assertFileExistsAndContent(false, new File(fromDir, "test_dir"), null);
        fromDir = new File(fromDir, "test_dir");
        assertFileExistsAndContent(true, new File(fromDir, "readme1.txt"), "readme1");
        assertFileExistsAndContent(true, new File(fromDir, "readme2.txt"), "readme2");
        assertFileExistsAndContent(false, new File(fromDir, "nested_test_dir"), null);
        fromDir = new File(fromDir, "nested_test_dir");
        assertFileExistsAndContent(true, new File(fromDir, "readme1.txt"), "readme1");
        assertFileExistsAndContent(true, new File(fromDir, "readme2.txt"), "readme2");
    }

    private static void assertFileExistsAndContent(boolean isFile, File file, String fileContentSnippet) {
        if (isFile) {
            Assert.assertTrue("No such file : " + file.getAbsolutePath(), file.isFile());
        } else {
            Assert.assertTrue("No such directory : " + file.getAbsolutePath(), file.isDirectory());
        }
        if (isFile) {
            byte[] fileBytes = IoUtils.readAllBytes(file);
            Assert.assertNotNull(fileBytes);
            Assert.assertTrue(new String(fileBytes).contains(fileContentSnippet));
        }
    }

    private static File getCopyTestBackupFilePath() {
        return new File(copyTestFromPath + ".bak");
    }

    private static void deleteFileIfExistsFailOnError(File toDelete, boolean isFailOnError) {
        if (!IoUtils.deleteFileIfExists(toDelete, isFailOnError)) {
            Assert.fail("Unable to delete file : " + toDelete);
        }
    }
}
