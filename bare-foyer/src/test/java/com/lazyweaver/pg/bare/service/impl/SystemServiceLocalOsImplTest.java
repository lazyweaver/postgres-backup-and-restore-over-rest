package com.lazyweaver.pg.bare.service.impl;

import com.lazyweaver.pg.bare.components.FilesystemManager.JobType;
import com.lazyweaver.pg.bare.components.JobManager;
import com.lazyweaver.pg.bare.dto.AbstractStatus.Status;
import com.lazyweaver.pg.bare.dto.OperationStatus;
import com.lazyweaver.pg.bare.repository.entity.JobEntity;
import com.lazyweaver.pg.bare.service.AbstractPostgreCommandsTest;
import org.junit.Assert;
import org.junit.Test;

public class SystemServiceLocalOsImplTest extends AbstractPostgreCommandsTest {

    @Test
    public void getDatabaseStatusForRunning() {
        prepareExecutable("pg_ctl.working");
        SystemServiceLocalOsImpl sut = new SystemServiceLocalOsImpl(null, null);
        OperationStatus status = sut.getDatabaseStatus();
        Assert.assertNotNull(status);
        Assert.assertEquals(Status.ACTIVE, status.getStatus());
    }

    @Test
    public void getDatabaseStatusForStopped() {
        prepareExecutable("pg_ctl.stopped");
        SystemServiceLocalOsImpl sut = new SystemServiceLocalOsImpl(null, new JobManagerDoNothingImpl());
        OperationStatus status = sut.getDatabaseStatus();
        Assert.assertNotNull(status);
        Assert.assertEquals(Status.INACTIVE, status.getStatus());
        Assert.assertTrue(status.getMessage().contains("pg_ctl: no server running"));
    }

    private static class JobManagerDoNothingImpl implements JobManager {

        @Override
        public JobEntity getJobEntity(String jobId) {
            return null;
        }

        @Override
        public void persist(JobEntity jobEntity) {}

        @Override
        public String getStorage(JobEntity entity) {
            return null;
        }

        @Override
        public String getStorage(String entityId) {
            return null;
        }

        @Override
        public OperationStatus remove(String entityId) {
            return null;
        }

        @Override
        public OperationStatus remove(JobEntity jobEntity) {
            return null;
        }

        @Override
        public void started(JobEntity jobEntity) {}

        @Override
        public JobEntity getLast(JobType jobType) {
            return null;
        }
    }
}
