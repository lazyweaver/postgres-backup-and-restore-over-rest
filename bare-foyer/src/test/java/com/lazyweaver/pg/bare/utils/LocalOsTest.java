package com.lazyweaver.pg.bare.utils;

import com.lazyweaver.pg.bare.utils.LocalOs.ExecCommandResult;
import org.junit.Assert;
import org.junit.Test;

public class LocalOsTest {

    @Test
    public void executeSingleCommandShouldFailsForNoSuchCommand() {
        ExecCommandResult result = null;
        result = LocalOs.execCommand("no_such");
        Assert.assertNotNull(result);
        Assert.assertFalse(result.isSuccess());
        Assert.assertTrue(result.getError().length() > 0);
        Assert.assertTrue(result.getOutput().isEmpty());
    }

    @Test
    public void executeSingleCommand() {
        ExecCommandResult result = LocalOs.execCommand(LocalOs.isWindows() ? "dir" : "ls -l");
        Assert.assertNotNull(result);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(result.getError().length() == 0);
        Assert.assertFalse(result.getOutput().isEmpty());
    }

    @Test
    public void executeMultipleCommands() {
        final String expectedOutput = LocalOs.isWindows() ? "Volume Serial Number is " : "total ";
        final String command = LocalOs.isWindows() ? "dir" : "ls -l";
        ExecCommandResult result = LocalOs.execCommand(createProgressLogger(), command, command);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(result.getError().length() == 0);
        Assert.assertFalse(result.getOutput().isEmpty());
        // Volume in drive C is System, Volume Serial Number is 3EA4-417A,
        int numOccur = 0;
        for (String line : result.getOutput()) {
            if (line.contains(expectedOutput)) {
                numOccur++;
            }
        }
        Assert.assertTrue(numOccur == 2);
        //
        result = LocalOs.execCommand(command, command, command);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(result.getError().length() == 0);
        Assert.assertFalse(result.getOutput().isEmpty());
        // Volume in drive C is System, Volume Serial Number is 3EA4-417A,
        numOccur = 0;
        for (String line : result.getOutput()) {
            if (line.contains(expectedOutput)) {
                numOccur++;
            }
        }
        Assert.assertTrue(numOccur == 3);
    }

    private ProgressLogger createProgressLogger() {
        return new ProgressLogger() {
            @Override
            public void log(String log) {
                System.out.println("[ProgressLogger from Test] " + log);
            }
        };
    }
}
