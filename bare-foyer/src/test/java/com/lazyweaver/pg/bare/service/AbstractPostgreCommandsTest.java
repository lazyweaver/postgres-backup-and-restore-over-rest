package com.lazyweaver.pg.bare.service;

import java.io.File;
import com.lazyweaver.pg.bare.utils.IoUtils;
import com.lazyweaver.pg.bare.utils.LocalOs;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;

public abstract class AbstractPostgreCommandsTest {

    @BeforeClass
    public static void initAll() {
        cleanAllTestScripts();
    }

    @After
    public void cleanAfterTest() {
        cleanAllTestScripts();
    }

    protected void prepareExecutable(String source) {
        File sourceFile = new File("src/test/resources/" + (LocalOs.isWindows() ? "windows" : "unix") + "/" + source);
        Assert.assertTrue("No such file :" + sourceFile.getAbsolutePath(), sourceFile.isFile());
        String execName = sourceFile.getName().substring(0, sourceFile.getName().indexOf('.'));
        File destFile = new File(getCurrentUserDIrectory(), LocalOs.isWindows() ? execName + ".bat" : execName);
        Assert.assertTrue("Can not copy from \"" + sourceFile.getAbsolutePath() + "\" to \"" + sourceFile.getAbsolutePath() + "\"", IoUtils.copyFile(sourceFile, destFile));
        Assert.assertTrue("Can not be set as executable:" + sourceFile.getAbsolutePath(), destFile.setExecutable(true));
    }

    private static void cleanAllTestScripts() {
        for (File script : getScriptsFromTests()) {
            if (!IoUtils.deleteFileIfExists(script, true)) {
                Assert.fail("Unable to delete file : " + script);
            }
        }
    }

    private static File[] getScriptsFromTests() {
        File userDir = getCurrentUserDIrectory();
        return new File[] {
                new File(userDir, LocalOs.isWindows() ? "pg_ctl.bat" : "pg_ctl"),
                new File(userDir, LocalOs.isWindows() ? "pg_dump.bat" : "pg_dump")};
    }

    private static File getCurrentUserDIrectory() {
        return new File(System.getProperty("user.dir"));
    }
}
