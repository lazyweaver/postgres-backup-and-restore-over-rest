package com.lazyweaver.pg.bare.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {

    @Test
    public void toInt() {
        Assert.assertTrue(StringUtils.toInt(null, 5) == 5);
        Assert.assertTrue(StringUtils.toInt("", 5) == 5);
        Assert.assertTrue(StringUtils.toInt(" ", 5) == 5);
        Assert.assertTrue(StringUtils.toInt("aa", 5) == 5);
        Assert.assertTrue(StringUtils.toInt("5", 5) == 5);
        Assert.assertTrue(StringUtils.toInt("15", 5) == 15);
        Assert.assertTrue(StringUtils.toInt("25", 5) == 25);
        Assert.assertTrue(StringUtils.toInt(" 25 ", 5) == 25);
    }

    @Test
    public void isUnixDirectoryCompliant() {
        Assert.assertFalse(StringUtils.isUnixDirectoryCompliant(null));
        Assert.assertFalse(StringUtils.isUnixDirectoryCompliant(""));
        Assert.assertFalse(StringUtils.isUnixDirectoryCompliant("    "));
        Assert.assertFalse(StringUtils.isUnixDirectoryCompliant("abc"));
        Assert.assertFalse(StringUtils.isUnixDirectoryCompliant("/"));// valid, but not allowed
        Assert.assertFalse(StringUtils.isUnixDirectoryCompliant("/root"));// valid, but not allowed
        Assert.assertFalse(StringUtils.isUnixDirectoryCompliant("/abc\\test"));
        Assert.assertTrue(StringUtils.isUnixDirectoryCompliant("/opt"));
        Assert.assertTrue(StringUtils.isUnixDirectoryCompliant("/opt/"));
        Assert.assertTrue(StringUtils.isUnixDirectoryCompliant("/opt/rc.d/test"));
        Assert.assertTrue(StringUtils.isUnixDirectoryCompliant("/opt/rc.d/test/abc455/only.123"));
        Assert.assertTrue(StringUtils.isUnixDirectoryCompliant("/usr/lib/libgccpp.so.1.0.2"));
    }

    @Test
    public void toAlphaLowercase() {
        Assert.assertNull(StringUtils.toAlphaLowercase(null));
        Assert.assertEquals("", StringUtils.toAlphaLowercase(""));
        Assert.assertEquals("", StringUtils.toAlphaLowercase("    "));
        Assert.assertEquals("abc", StringUtils.toAlphaLowercase("abc"));
        Assert.assertEquals("abc", StringUtils.toAlphaLowercase("abc1"));
        Assert.assertEquals("abc", StringUtils.toAlphaLowercase("2a5bc1"));
        Assert.assertEquals("abcdefone", StringUtils.toAlphaLowercase("abc(def) one"));
        Assert.assertEquals("abcdefone", StringUtils.toAlphaLowercase("abc(Def) One"));
    }
}
