# Postgre Database Server & Tools

## Project Description
Description coming up soon...

![Overview of solution](docs/arch.png "Overview of solution")

## Docker image contains

- [Alpine](https://hub.docker.com/_/alpine "Alpine") base image - 3.12
- [Postgre](https://github.com/docker-library/postgres/blob/1d140375b6830c65cfeaac3642c7fda6d3e1b29a/10/alpine/Dockerfile "Postgre") database server - **10.14**, other versions coming up soon...
- Java 13 available in image (required by Backup Restore application)
- Backup/Restore scripts - Full, incremental and custom (invoked by Backup Restore application)
- [Backup/Restore REST APIs](bare-foyer "Backup/Restore REST APIs") - Full Backup, Full Restore, Partial Backup, Partial Restore, PITR Restore

### Docker image build
```
cd bare-foyer
mvn clean package -DskipTests
cp target/bare-foyer-*.jar ../docker/tools
cd ../docker
docker build -t postgres10.14-bare:0.0.1 .
```

### Docker container run
```
docker run -d -p 5432:5432 -p 9001:9001 -p 25001:25001 \
--memory="2G" --cpus="1" \
-e POSTGRES_PASSWORD=postgres \
-e PGDATA=/var/lib/postgresql/data/pgdata \
-e PGLOGSFILE=/opt/logs/server.log \
-e BF_ERR_LOGS_FILE=/opt/logs/bare_foyer_errors.log \
-e BF_DEBUG_APP=true \
-e postgreDataDir=/var/lib/postgresql/data/pgdata \
-e postgreToolsDir=/home/postgres/tools/postgres \
-e postgreLogsRemoteDir=/some/persistent/storage/dir/logs \
-e postgreBackupRemoteDir=/some/persistent/storage/dir/backup \
-e securityRemoteAccessApiKey=developerTest123 \
-v /some/persistent/storage/dir/data:/var/lib/postgresql/data \
-v /some/persistent/storage/dir/logs:/opt/logs \
-v /some/persistent/storage/dir/backup:/opt/backup \
--hostname postgres-bare \
--name postgres-bare \
postgres10.14-bare:0.0.1
```
### Utilization DB access
```
Postgres DB : localhost:5432 (or user-defined value)
Postgres User : postgres
Postgres User password : postgres (or user-defined value)
```
### Utilization REST API
REST service endpoints (Swagger UI):
http://localhost:9001/swagger-ui.html#/
// 9001 port can be a user-defined value

**Backup**
**Backup Controller**

**GET**/backup/full
Provides information of the Last "Full Backup" job

**POST**/backup/full
Creates a new "Full Backup" job

**GET**/backup/full/{jobId}
Provides information of a "Full Backup" job by ID

**GET**/backup/partial
Provides information of the Last "Partial Backup" job

**GET**/backup/partial/{jobId}
Provides information of a "Partial Backup" job by ID

**GET**/backup/partial/active
Gets all active "Partial Backup" jobs

**POST**/backup/partial/schema/{schemaName}
Creates a new "Partial Backup" job for "schema"

**POST**/backup/partial/table/{tableName}
Creates a new "Partial Backup" job for "table"

**Restore**
**Restore Controller**

**GET** /restore/full
Provides information of the Last "Full Restore" job

**GET** /restore/full/{jobId}
Provides information of a "Full Restore" job by ID

**POST**/restore/full/{sourceJobId}
Creates a new "Full Restore" job using a "Full Backup" job ID

**GET**/restore/partial
Provides information of the Last "Partial Restore" job

**GET**/restore/partial/{jobId}
Provides information of a "Partial Restore" job by ID

**POST**/restore/partial/{sourceJobId}
Creates a new "Partial Restore" job using a "Partial Backup" job ID

**GET**/restore/partial/active
Gets all active "Partial Restore" jobs

**GET**/restore/pitr
Provides information of the Last "Point-in-Time Recovery" job

**GET**/restore/pitr/{jobId}
Provides information of a "Point-in-Time Recovery" job by ID

**POST**/restore/pitr/{recoveryDateTime}
Creates a new "Point-in-Time Recovery" job for the requested date "dd-MM-yyyy HH:mm"

**System**
**System Controller**

**GET**/system/bare-foyer
Provides the status of Bare Foyer Service and directories

**GET**/system/database
Provides the status of PostgreSQL Server

**GET**/system/job/{jobId}
Provides information on any job by ID

**DELETE**/system/job/{jobId}
Deletes all related data and the job by ID

### Container Helper commands
```
# View container last logs
docker logs -f --tail "50" postgres-bare
# Login to container
docker exec -it postgres-bare bash

# Start/Stop Postgre from inside container
docker exec -it postgres-bare bash
pg_ctl -D "/var/lib/postgresql/data/pgdata" -l /opt/logs/server.log start
pg_ctl -D "/var/lib/postgresql/data/pgdata" -w stop
```

###  Backup & Restore (How it works behind the scenes)

####  Backup Full
```
# create the backup
pg_basebackup --wal-method=stream --gzip --format=tar --pgdata=/opt/backup/2020-07-13 --progress --username=postgres --no-password 
# ex. (outputs : 46154/46154 kB (49%), 1/1 tablespace)
# validate the archives
tar -tzf /opt/backup/2020-07-13/base.tar.gz
tar -tzf /opt/backup/2020-07-13/pg_wal.tar.gz
# or skip the "stdout" output
tar -tzf /opt/backup/2020-07-13/base.tar.gz > /dev/null
tar -tzf /opt/backup/2020-07-13/pg_wal.tar.gz > /dev/null
```
#### Recover Full
```
# stop PostgreSQL server
pg_ctl -D "/var/lib/postgresql/data/pgdata" -w stop
# Remove the content of Data directory
rm -rf /var/lib/postgresql/data/pgdata/*
# Remove the logs
rm -rf /opt/logs/*
# Add the backup files
tar -xzvC /var/lib/postgresql/data/pgdata -f /opt/backup/2020-07-13/base.tar.gz
# Add the WAL files
tar -xzvC /var/lib/postgresql/data/pgdata/pg_wal -f /opt/backup/2020-07-13/pg_wal.tar.gz
# Assign ownership
chown -R postgres:postgres /var/lib/postgresql/data/pgdata
# start PostgreSQL server
pg_ctl -D "/var/lib/postgresql/data/pgdata" -l /opt/logs/server.log start
```

####  Backup Incremental (Automatic)
```
archive_mode = on
# Continuously copy generated WAL files to the NAS archive mounted location - automated by Postgre. Considered fast operation.
# PostgreSQL does not know how we want to archive the WAL files, we have to supply a small script. PostgreSQL will invoke this script when each WAL file is ready for archiving.
archive_command = '/home/postgres/tools/archive_wal.sh "%p" "%f" >> /opt/logs/wal_archiving.log 2>&1'
```
Check the WAL archiving information via SQL:
```
SELECT * FROM pg_stat_archiver
```

####  Recover Point In Time (Full + PITR Incremental)

PITR refers to restoring the PostgreSQL DB to the state it was at a particular point in time. Steps:
- Perform "Recover Full" (see "Recover Full") - except for Start server step
- Create a $PGDATA/recovery.conf file that has the contents:
```
# Mounted NAS directory - /opt/backup
restore_command = '/home/postgres/tools/postgres/restore_wal.sh "%p" "%f" >> /opt/logs/wal_restore.log 2>&1'
# Desired time to recovery to
recovery_target_time = '2020-07-13 11:20:00 UTC'
recovery_target_inclusive = false
```
- Start server
```
pg_ctl -D "/var/lib/postgresql/data/pgdata" -l /opt/logs/server.log start
```

####  Backup Partial (ex. Schema)
```
# no compression
pg_dump -d testDb -n '"testSchema"'  --verbose > /opt/backup/testDb_testSchema.sql
# with compression - tar.gz
pg_dump -d testDb -n '"testSchema"' -F c --verbose > /opt/backup/testDb_testSchema.sql.tar.gz
```