#!/usr/bin/env bash
checkFileWal='/home/postgres/working/postgre_wal_archiving_config'
if [ ! -f "$checkFileWal" ]; then
	>&2 echo 'WAL Archiving not activated'
	exit 1
fi
backupDir=$(cat $checkFileWal)
if [ ! -d "$backupDir" ]; then
	>&2 echo "Backup directory does not exist : ${backupDir}"
	exit 1
fi
backupDir="$backupDir/WAL"
if [ ! -d "$backupDir" ]; then
	echo "Backup WAL directory does not exist : ${backupDir} will try to create it"
	mkdir $backupDir
	echo "Backup WAL directory created : ${backupDir}"
fi
walFilePath="$backupDir/$2"
if [ -f "$walFilePath" ]; then
	echo "$walFilePath exists as a backup, will try to remove it"
	rm -f $walFilePath
	echo "$walFilePath removed"
fi
cp $1 $walFilePath
if [ $? -ne 0 ];then
	exit 1
fi
echo "Copied WAL file at $(date). Params : '$1', '$2'"
exit 0