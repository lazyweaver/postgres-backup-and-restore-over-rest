#!/usr/bin/env bash

stopDatabaseServerOrExit(){
	# stop PostgreSQL server
	local statusSrv=$(pg_ctl status)
	echo $statusSrv
	if [ "$statusSrv" = 'pg_ctl: no server running' ];then
		echo 'PostgreSQL server is already stopped, nothing to do.'
	else
		pg_ctl -D "$1" -w stop
		if [ $? -ne 0 ];then
			exitScript "error" "Unable to stop PostgreSQL server" $2
		fi
		echo 'PostgreSQL server stopped'
	fi
}

startDatabaseServer(){
	# start PostgreSQL server
	pg_ctl -D "$1" -l $2 start
	if [ $? -ne 0 ];then
		exitScript "error" "Unable to start PostgreSQL server" $3
	fi
	echo 'PostgreSQL server started'
}

removeDirectoryContent(){
	echo "Will start removing content from directory $1/*"
	rm -rf $1/*
	if [ $? -ne 0 ];then
		exitScript "error" "Unable to remove content from $1/*" $2
	fi
	echo "Removed the content of directory $1/*"
}

removeLogsContent(){
	# Remove the CSV and other logs
	rm -rf $1/*.csv
	rm -rf $1/*.log
	echo "Removed the CSV and log content of Logs directory $1/*"
}

extractArchive(){
	echo "Will start adding $3 files..."
	tar -xzvC $1 -f $2
	if [ $? -ne 0 ];then
		exitScript "error" "Unable to extract $3 content from $2 to $1" $4
	fi
	echo "Backup $3 files added to $1"
}

assignPostgreUserAndGroupOwnership(){
	chown -R postgres:postgres $1
	if [ $? -ne 0 ];then
		exitScript "error" "Unable to change recursive ownership to postgres:postgres of $1" $2
	fi
	echo "Ownership of data directory changed"
}

stopServerAndPerformFullRestore() {
	echo 'Full restore started'
	# validate the archives
	echo 'Validate the archives'
	validateArchive "$2/base.tar.gz" $4
	validateArchive "$2/pg_wal.tar.gz" $4
	echo 'Archives validated'
	stopDatabaseServerOrExit "$1" $4
	# Remove the content of Data directory
	removeDirectoryContent "$1" $4
	# Remove logs
	removeLogsContent $3
	# Add the backup files
	extractArchive "$1" "$2/base.tar.gz" "Base" $4
	# Remove obsolete Wal from Data directory
	removeDirectoryContent "$1/pg_wal" $4
	# extract streamed archivel WAL during backup
	extractArchive "$1/pg_wal" "$2/pg_wal.tar.gz" "WAL" $4
	# Assign ownership
	assignPostgreUserAndGroupOwnership "$1" $4
	echo 'Full restore ended'
}

getWalArchivingConfig(){
	local walArchConfigFile='/home/postgres/working/postgre_wal_archiving_config'
	if [ ! -f "$walArchConfigFile" ]; then
		exitScript "error" "WAL Archiving not activated for PostgreSQL" $1
	fi
	echo $walArchConfigFile
}

validateDirectoryExists(){
	if [ ! -d "$1" ]; then
		exitScript "error" "Directory does not exist : $1" $2
	fi
}

getWalBackupDirectory(){
	local walArchConfigFile=$(getWalArchivingConfig $1)
	if [ "$walArchConfigFile" == "$MSG_JOB_ERROR" ]; then
		exitScript "error" "Unable to get WAL archiving config file due to a previous error" $1
	fi
	local backupRootDir=$(cat $walArchConfigFile)
	if [ "$backupRootDir" == "$MSG_JOB_ERROR" ]; then
		exitScript "error" "Unable to read WAL archiving config file saved value due to a previous error" $1
	fi
	validateDirectoryExists "$backupRootDir" $1
	backupRootDir="$backupRootDir/WAL"
	validateDirectoryExists "$backupRootDir" $1
	echo $backupRootDir
}
