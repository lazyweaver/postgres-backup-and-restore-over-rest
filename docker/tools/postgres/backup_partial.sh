#!/usr/bin/env bash

source /home/postgres/tools/postgres/includable_basic_functions.sh

_main() {
	echo 'Partial Backup started'
	tarFilePath="$4/dump.tar"
	archiveFilePath="$tarFilePath.gz"
	echo "Will try to create tar under $tarFilePath"
	cmd="pg_dump -d $1 -$2 '$3' -F t --verbose > $tarFilePath"
	echo "cmd=>$cmd"
	eval $cmd
	if [ $? -ne 0 ];then
	   exitScript "error" "Unable to create tar under $tarFilePath" $5
	fi
	echo "TAR archive created under $tarFilePath"
    echo "TAR Archive will be compressed"
	gzip < $tarFilePath > $archiveFilePath
	rm $tarFilePath

	# Validate the archive
	echo "Will try to validate compressed archive under $archiveFilePath"
	validateArchive $archiveFilePath $5
	echo "Archive validated - $archiveFilePath"
	echo 'Partial Backup ended'
	exitScript "success" "Partial backup created and validated" $5
}

_main "$@"