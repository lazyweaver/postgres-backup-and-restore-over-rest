#!/usr/bin/env bash

source /home/postgres/tools/postgres/includable_basic_functions.sh

_main() {
	echo 'Full Backup started'
	pg_basebackup --wal-method=stream --gzip --format=tar --pgdata=$1 --progress --username=postgres --no-password
	if [ $? -ne 0 ];then
		exitScript "error" "pg_basebackup failed to create archive under $1" $2
	fi
	# validate the archives
	echo 'Validate the archives'
	validateArchive "$1/base.tar.gz" $2
	validateArchive "$1/pg_wal.tar.gz" $2
	echo 'Archives validated'	
	echo 'Full Backup ended'
	exitScript "success" "Full backup created and validated $1" $2
}

_main "$@"