#!/usr/bin/env bash
MSG_JOB_ERROR='Job ended with success'

exitScript(){
	echo "end_status=$1" >> $3
	echo "end_message=$2" >> $3
	echo "date_ended=$(env TZ=Europe/Sofia date +"%d-%m-%Y %H:%M:%S")" >> $3
	if [ "$1" == 'success' ]; then
		echo $MSG_JOB_ERROR
		exit 0
	fi
	echo 'Job ended with error'
	exit 1
}

validateArchive(){
	tar -tzf $1 > /dev/null
	if [ $? -ne 0 ];then
		exitScript "error" "Unable to validate archive $1" $2
	fi
}
