#!/usr/bin/env bash

source /home/postgres/tools/postgres/includable_basic_functions.sh
source /home/postgres/tools/postgres/includable_restore_functions.sh

createRecoveryConfFile(){
	recoveryConfFile="$1/recovery.conf"
	if [ -f "$recoveryConfFile" ]; then
		echo "WARNING: A previous recovery file exists $recoveryConfFile !!!"
		rm $recoveryConfFile
		echo "The previous recovery file removed $recoveryConfFile"
	fi
	echo "restore_command = '/home/postgres/tools/postgres/restore_wal.sh \"$2/%f\" \"%p\" >> /opt/logs/wal_archiving.log 2>&1'" > $recoveryConfFile
	echo "recovery_target_time = '$3'" >> $recoveryConfFile
	echo 'recovery_target_inclusive = false' >> $recoveryConfFile
	# debug recovery.conf
	echo "A new recovery file created $recoveryConfFile"
	cat $recoveryConfFile 
}

_main() {
	echo 'PITR restore started'
	local walBackupDirectory=$(getWalBackupDirectory $5)
	if [ "$walBackupDirectory" == "$MSG_JOB_ERROR" ]; then
		exitScript "error" "Unable to retrieve WAL backup directory value due to a previous error" $5
	fi
	echo "WAL Archiving is active by configuration. WAL archive path - $walBackupDirectory"
	stopServerAndPerformFullRestore "$@"
	createRecoveryConfFile "$1" "$walBackupDirectory" "$4" $5
	startDatabaseServer "$1" "$3/server.log" $5
	echo 'PITR restore ended'
	exitScript "success" "PITR restore completed - Full restore from $2 and WAL to date '$4'" $5
}

_main "$@"