#!/usr/bin/env bash
restore_wall_file(){
	if [ -f "$1" ]; then
		echo "$1 exists in backup"
	else
		>&2 echo "$1 does NOT exist in backup"
		exit 1
	fi
	cp $1 $2
	echo "Restored WAL file at $(date). Params : '$1', '$2'"
}
restore_wall_file "$1" "$2"
exit 0