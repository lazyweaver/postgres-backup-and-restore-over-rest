#!/usr/bin/env bash
checkFileCronY='/home/postgres/working/clean_postgre_logs_yes'
if [ -f "$checkFileCronY" ]; then
	echo 'Clean logs cron has been set to Yes'
	logsDir=$(cat $checkFileCronY)
	find $logsDir/*.* -type f -mmin +60 -exec rm {} \;
	echo "Cleaned all logs from ${logsDir}"
else
	echo 'Clean logs cron has NOT been set to Yes, will not clean anything'
fi
