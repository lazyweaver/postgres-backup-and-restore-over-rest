#!/usr/bin/env bash

source /home/postgres/tools/postgres/includable_basic_functions.sh
source /home/postgres/tools/postgres/includable_restore_functions.sh

_main() {
	stopServerAndPerformFullRestore "$@"
	startDatabaseServer "$1" "$3/server.log" $4
	exitScript "success" "Full restore performed from $2" $4
}

_main "$@"