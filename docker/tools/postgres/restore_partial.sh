#!/usr/bin/env bash

source /home/postgres/tools/postgres/includable_basic_functions.sh

_main() {
	echo 'Partial restore started'
	# validate the archive
	echo 'Validate the archive'
	validateArchive $2/dump.tar.gz $4
	echo 'Archive validated, will start extracting it...'
	gunzip -c $2/dump.tar.gz > $2/dump.tar
	echo 'TAR extracted, will validate it...'
	tar -tf $2/dump.tar > /dev/null
	echo 'TAR validated, will start pg_restore...'
	pg_restore -d $1 $2/dump.tar -F t -c --username=postgres --no-password
	if [ $? -ne 0 ];then
		testWarning=$(grep 'WARNING: errors ignored on restore' $3)
		if [[ ${testWarning:0:7} = 'WARNING' ]]; then
			echo $testWarning
		else
			rm $2/dump.tar
			echo "Temporary TAR removed $2/dump.tar"
			exitScript "error" "Unable to perform pg_restore from archive $2/dump.tar.gz" $4
		fi
	fi
	rm $2/dump.tar
	echo "Temporary TAR removed $2/dump.tar"
	echo 'Partial restore ended'
	exitScript "success" "Partial restore performed from $2/dump.tar.gz to database $1" $4
}

_main "$@"